# Installation
- composer require propertywebmasters/journey-theme

# Publish Assets (Required)
- php artisan vendor:publish --tag=journey-theme-assets

# Publish Blade Files
- php artisan vendor:publish --tag=journey-theme-views
