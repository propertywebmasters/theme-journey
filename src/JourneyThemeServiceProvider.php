<?php

namespace PropertyWebmasters\JourneyTheme;

use Illuminate\Support\ServiceProvider;

class JourneyThemeServiceProvider extends ServiceProvider
{
    private const THEME = 'journey';

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->loadViewsFrom(__DIR__, self::THEME.'-theme');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/config/' => base_path('resources/views/vendor/themes/'.self::THEME.'/config'),
            __DIR__ . '/views/' => base_path('resources/views/vendor/themes/'.self::THEME.'/views'),
        ], self::THEME.'-theme-views');
        
        $this->publishes([
            __DIR__ . '/assets/' => public_path('themes/'.self::THEME.'/assets'),
        ], self::THEME.'-theme-assets');
    }
}
