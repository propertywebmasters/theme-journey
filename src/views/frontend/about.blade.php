@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <section id="about" class="center-cover-bg bg-lazy-load relative" data-style="{{ backgroundCSSImage('about.hero') }}">
        <div class="py-64 px-4 sm:px-10 lg:px-0">
            <div class="container mx-auto transparent text-left">
                <div class="mb-6 md:mb-16">
                    <h1 class="text-white text-5xl lg:text-6xl font-medium mx-auto mb-4">{!! translatableContent('about', 'about-title') !!}</h1>
                    <p class="text-white">{{ translatableContent('about', 'about-subtitle') }}</p>
                </div>

                <div class="flex flex-row">
                    <div class="mr-2">
                        <a href="{{ localeUrl('/meet-the-team') }}" id="meet-the-team-button" class="primary-bg px-3 sm:px-8 py-4 text-white rounded block md:inline-block mb-3 md:mb-0 border primary-border">{{ trans('header.meet_the_team') }}</a>
                    </div>
                    <div>
                        <a href="{{ localeUrl('/contact') }}" id="our-branches-button" class="border px-3 sm:px-8 py-4 text-white rounded block md:inline-block">{{ trans('generic.our_branches') }}</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="absolute bottom-0 left-0 w-full">
            @include(themeViewPath('frontend.components.banners.property-search-banner'), ['opacity' => '0.65'])
        </div>
    </section>

    <div class="pt-12 lg:py-20 pb-4">
        <div class="container mx-auto">

            <div class="grid grid-cols-1 gap-8">
                <div class="px-10 lg:px-32">
                    <h3 class="font-medium pb-6 text-center header-text" style="font-size: 2.5rem;">{!! translatableContent('about', 'about-section-1-title') !!}</h3>
                    <p class="text-center text-base">{!! translatableContent('about', 'about-section-1-text') !!}</p>
                </div>
            </div>

            {{-- @php
                $items = [
                    [
                        'date' => '1958',
                        'text' => 'We were founded by John Bramley who was already a practicing Chartered Surveyor. He opened our long standing flagship office in the heart of Huddersfield.',
                        'image' => themeImage('timeline/1958.jpg'),
                        'additionalClasses' => [
                            'container' => 'mb-48 lg:mb-0',
                            'image' => 'left-4'
                        ],
                    ],
                    [
                        'date' => "60/70's",
                        'text' => 'In this period, there was a high volume of properties to rent and very few were owner occupied. Our business concentrated on the commercial and residential lettings side.',
                    ],
                    [
                        'date' => "1976",
                        'text' => 'Following an increase in home ownership, Peter Butler joined Bramleys as a Partner to expand residential sales. Joe Bramley also joined the business.',
                    ],
                    [
                        'date' => "1984",
                        'text' => 'John Bramley retired. At this time, Bramleys was one of three independent agents in Huddersfield.',
                        'additionalClasses' => [
                            'container' => 'lg:-mt-8'
                        ],
                    ],
                    [
                        'date' => "Mid 80s",
                        'text' => 'There were changes made to the financial act in the mid 1980’s that allowed banks to do mortgages and open as estate agents. Large corporate agents entered the market.',
                        'additionalClasses' => [
                            'text' => 'pl-30',
                        ],
                    ],
                    [
                        'date' => "1990",
                        'text' => 'Andrew Moorhouse joined the partnership to provide additional support to the surveying department.',

                    ],
                    [
                        'date' => "1990",
                        'text' => 'Unfortunately, the Huddersfield office had a fire in 1990 that spread from adjoining business premises. A full office renovation took place.',
                        'image' => themeImage('timeline/1990.jpg'),
                        'additionalClasses' => [
                            'container' => 'mb-48 lg:mb-0',
                            'image' => 'left-4'
                        ],
                    ],
                    [
                        'date' => "1993",
                        'text' => 'Paul Keighley joined Bramleys in 1993 to drive the expansion of residential sales.',
                        'additionalClasses' => [
                            'container' => 'lg:-mt-16'
                        ],
                    ],
                    [
                        'date' => "1998",
                        'text' => 'Paul Keighley and Alex McNeil become partners of the business, joining Andrew Moorhouse and Peter Butler',
                        'additionalClasses' => [
                            'container' => 'lg:mt-32'
                        ],
                    ],
                    [
                        'date' => "1999",
                        'text' => 'Bramleys opened our second office in Halifax Town Centre, conveniently on St Georges Square to match the Huddersfield office.',
                    ],
                    [
                        'date' => "2007",
                        'text' => 'Peter Butler retires and becomes a consultant.',
                    ],
                    [
                        'date' => "2007",
                        'text' => 'Graeme Haigh and Helen Hollingsworth join and become partners. Elland branch opens.',
                    ],
                    [
                        'date' => "2008",
                        'text' => 'Banking crisis hits.',
                        'image' => themeImage('timeline/2008.jpeg'),
                        'additionalClasses' => [
                            'container' => 'mb-48 lg:mb-0',
                            'image' => 'left-4'
                        ],
                    ],
                    [
                        'date' => "2016",
                        'text' => 'Brexit - EU Referendum. UK won majority to leave to european union by 52% to 48%.',
                        'additionalClasses' => [
                            'container' => 'lg:-mt-16'
                        ],
                    ],
                    [
                        'date' => "2020",
                        'text' => 'The world was hit by the Covid-19 pandemic which surprisingly resulted in a surge of demand for property.',
                        'additionalClasses' => [
                            'container' => 'lg:mt-28'
                        ],
                    ],
                    [
                        'date' => "2021",
                        'text' => 'Jonathan Wilson becomes a partner. Andrew Moorhouse becomes a consultant.',
                        'additionalClasses' => [
                            'container' => 'lg:-mt-8'
                        ],
                    ],
                    [
                        'date' => "2021",
                        'text' => 'Winning 10 awards across our business at the allAgents awards.',
                        'image' => themeImage('timeline/2021.jpg'),
                        'additionalClasses' => [
                            'container' => 'mb-48 lg:mb-0',
                            'image' => 'left-4'
                        ],
                    ],
                    [
                        'date' => "2022",
                        'text' => 'Huddersfield office renovation.',
                        'additionalClasses' => [
                            'date' => '-mt-4',
                        ],
                    ],
                ];
            @endphp

            <div class="px-8 sm:px-40 md:px-60 lg:px-10 pt-12 pb-2">
                @include(themeViewPath('frontend.components.timeline'), ['items' => $items])
            </div> --}}
        </div>
    </div>

    <section>
        <div class="container mx-auto px-4 py-20 lg:px-32 {{ !empty(trim(translatableContent('about', 'about-section-2-title'))) && !empty(trim(translatableContent('about', 'about-section-2-text'))) ? '' : 'hidden' }}">
            <div class="text-center lg:px-32">
                <div class="mb-16">
                    <h1 class="header-text pb-6" style="font-size: 2.5rem;">{!! translatableContent('about', 'about-section-2-title') !!}</h1>
                    <p>{!! translatableContent('about', 'about-section-2-text') !!}</p>
                </div>
            </div>
        </div>
    </section>

    {{-- <section class="secondary-bg text-white p-8 py-20">
        <div class="container mx-auto">
            <div class="grid grid-cols-1 lg:grid-cols-2 gap-16">
                <div class="text-sm">
                    <h3 class="font-medium pb-6 mb-8 text-2xl text-center md:text-left border-b header-text">{!! translatableContent('about', 'about-main-title') !!}</h3>
                    <p class="text-center md:text-left">{!! translatableContent('about', 'about-main-text') !!}</p>
                </div>
                <div class="hidden lg:block">
                    <img class="w-full" src="{{ assetPath(translatableContent('about', 'about-main-image')) }}" alt="image">
                </div>
            </div>
        </div>
    </section> --}}

    @include(themeViewPath('frontend.components.testimonials'))

    @include(themeViewPath('frontend.components.latest-news'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
