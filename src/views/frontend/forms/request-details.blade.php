<form action="{{ localeUrl('/property/'.$property->url_key.'/enquiry') }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
    @csrf
    <input type="text" name="name" placeholder="Full name" class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
    <input type="email" name="email" placeholder="Email address" class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
    <input type="text" name="tel" placeholder="Telephone number" class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
    <textarea name="comment" id="" cols="30" rows="10" placeholder="Further comments" class="rounded-sm bg-whiter h-32 w-full px-4 py-3 mb-3 font-light text-black"></textarea>
    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light'])
    <div class="text-center border-b-2 pb-8 mb-8">
        <button type="submit" class="text-sm text-center tracking-wide rounded border max-w-xs inline-block py-3 px-8 transition-all hover:text-white duration-500 cta cta-text">{{ trans('button.enquire_now') }}</button>

        @if (shouldShowWhatsappCta())
            <div class="my-2 text-center">
                <span class="uppercase">{{ trans('generic.or') }}</span>
            </div>

            @include(themeViewPath('frontend.components.whatsapp-cta'), ['property' => $property])
        @endif
    </div>
</form>
