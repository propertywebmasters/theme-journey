<form method="post" action="{{ localeUrl('/contact') }}" class="recaptcha">
    <div class="">

        <div class="bg-whiter rounded mb-4">
            <input class="block w-full focus:outline-none bg-transparent border-b border-white p-4" type="text" name="name" placeholder="{{ trans('contact.full_name') }} *" required>
        </div>

        <div class="bg-whiter rounded mb-4">
            <input class="block w-full focus:outline-none bg-transparent border-b border-white p-4" type="email" name="email" placeholder="{{ trans('contact.email_address') }} *" required>
        </div>

        <div class="bg-whiter rounded mb-4">
            <input class="block w-full focus:outline-none bg-transparent border-b border-white p-4" type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}">
        </div>

        <div class="bg-whiter rounded mb-4">
            <textarea class="block w-full focus:outline-none bg-transparent border-b border-white p-4 h-40" name="message" placeholder="{{ trans('contact.your_message') }} *" required></textarea>
        </div>

        <div class="bg-whiter rounded mb-8">
            @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'block w-full focus:outline-none bg-transparent border-b border-white p-2'])
        </div>

        <div class="text-center">
            <button type="submit" role="button" class="capitalize-first-letter inline primary-bg text-center py-4 px-8 rounded leading-normal tracking-wide cursor-pointer w-auto hover-lighten text-white">
                @if (isset($buttonLabel))
                    {{ $buttonLabel }}
                @else
                    {{ trans('contact.get_in_touch') }}
                @endif
            </button>
        </div>


    </div>
    @csrf
</form>
