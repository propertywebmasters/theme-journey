@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    @php
    use App\Models\TenantFeature;$multiColumn = hasFeature(TenantFeature::FEATURE_GENERIC_CONTACT_FORM);
    $breadcrumb['/'] = trans('header.home');

    if ($page->parent !== null) {
        $breadcrumb['/'.$page->parent->url_key] = $page->parent->title;
    }

    $breadcrumb['#'] = $page->title;

    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'))
    @endphp

    <section id="generic-page" class="bg-whiter pb-12 pt-32 lg:pb-16 lg:pt-36">
        <div class="container px-4 mx-auto xl:px-0">
            @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

            <div class="flex flex-col lg:flex-row lg:items-center lg:justify-between">
                <div class="mb-4 lg:mb-0">
                    <h1 class="text-4xl pb-4 pb-2 lg:pb-8">{!! $page->title !!}</h1>

                    <!--  Breadcrumb -->
                    <div class="generic-breadcrumb">
                        @php $i = 1 @endphp
                        @foreach($breadcrumb as $url => $anchor)
                            <a class="cta-bg-text-only" href="{{ $url }}">{!! $anchor !!}</a>
                            @if ($i < count($breadcrumb)) &gt; @endif
                            @php $i++ @endphp
                        @endforeach
                    </div>
                </div>

                @if (isset($searchControls->tender_type))
                    <div class="flex items-center lg:justify-end">
                        @if ($searchControls->tender_type === 'sale_only')
                            <a id="valuation-cta" class="rounded inline-block leading-loose text-center tracking-wide font-bold primary-bg text-white md:py-3 py-2 px-2 lg:px-12 transition-all" href="{{ localeUrl('/all-properties-for-sale') }}">
                                {{ trans('search.search_sales') }}
                            </a>
                        @elseif ($searchControls->tender_type === 'rental_only')
                            <a id="valuation-cta" class="rounded inline-block leading-loose text-center tracking-wide font-bold primary-bg text-white md:py-3 py-2 px-2 lg:px-12 transition-all" href="{{ localeUrl('/all-properties-for-rent') }}">
                                {{ trans('search.search_rentals') }}
                            </a>
                        @else
                            <a id="valuation-cta" class="rounded inline-block leading-loose text-center tracking-wide font-bold primary-bg text-white md:py-3 py-2 px-2 lg:px-12 transition-all mr-2" href="{{ localeUrl('/all-properties-for-rent') }}">
                                {{ trans('search.search_rentals') }}
                            </a>

                            <a id="valuation-cta" class="rounded inline-block leading-loose text-center tracking-wide font-bold secondary-bg text-white md:py-3 py-2 px-2 lg:px-12 transition-all " href="{{ localeUrl('/all-properties-for-sale') }}">
                                {{ trans('search.search_sales') }}
                            </a>
                        @endif
                    </div>
                @endif
            </div>
        </div>
    </section>

    <section class="py-8">
        <div class="container px-4 mx-auto xl:px-0">
            <div class="flex flex-wrap">
                @php
                    $width = $multiColumn ? 'lg:w-4/6' : ''
                @endphp
                <div class="w-full {{ $width }} pr-0 md:pr-8">
                    <div class="mb-16">
                        <h3 class="text-2xl leading-loose tracking-tight font-bold text-primary">{{ $page->subtitle }}</h3>
                        @if ($page->image)
                            <img src="{{ assetPath($page->image) }}" class="w-full mb-6">
                        @endif

                        <div class="generic-page-content">{!! parseContentForShortcodes($page->content) !!}</div>
                    </div>
                </div>

                @if ($multiColumn)
                    <div class="w-full lg:w-2/6">
                        <div class="bg-whiter">
                            @if($pageNavigation->count() > 0)
                                <div class="w-full primary-bg text-white text-base pr-2 mb-4">
                                    <form>
                                        <select id="sort" name="sort" class="autojump w-full block focus:outline-none leading-normal text-base primary-bg text-white p-2">
                                            @foreach($pageNavigation as $navigation)
                                                @php $selected = trim($_SERVER['REQUEST_URI'], '/') === $navigation->url_key ? 'selected="selected"' : '' @endphp
                                                <option class="primary-bg text-white" value="{{ url($navigation->url_key) }}" {{ $selected }}>{{ $navigation->title }}</option>
                                            @endforeach
                                        </select>
                                    </form>
                                </div>
                            @endif

                            <div class="py-9 px-4 lg:px-7 generic-contact-form">
                                <h3 class="text-2xl leading-normal text-center tracking-tight font-bold text-primary mb-7">{{ trans('contact.get_in_touch') }}</h3>
                                <form action="{{ url()->current() }}" method="post" enctype="application/x-www-form-urlencoded" class="recaptcha">
                                    <input type="text" name="name" placeholder="Full name" class="border h-14 w-full px-4 mb-3">
                                    <input type="email" name="email" placeholder="Email address" class="border h-14 w-full px-4 mb-3">
                                    <input type="text" name="tel" placeholder="Telephone number" class="border h-14 w-full px-4 mb-3">
                                    <textarea name="message" id="" cols="30" rows="10" placeholder="Further comments" class="border h-32 w-full px-4 py-3 mb-3"></textarea>

                                    <input type="hidden" name="url" value="{{ url()->current() }}">
                                    @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'border h-14 w-full px-4 mb-3'])

                                    <div class="text-center">
                                        <button type="submit" class="text-base text-center tracking-wide font-bold text-white uppercase px-4 sm:px-9 h-9 sm:h-12 right-1 top-1 hover:bg-secondary transition-all uppercase primary-bg rounded capitalize-first-letter">{{ trans('button.send_enquiry') }}</button>
                                    </div>
                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                @endif
            </div>

        </div>
    </section>

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
