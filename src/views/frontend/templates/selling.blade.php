@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="selling">
        <div class="py-32 sm:py-40 lg:py-64 relative overflow-hidden">
            <div class="container mx-auto text-left z-30">
                <div class="lg:mb-8">
                    {{-- <h1 class="text-white text-6xl font-medium mx-auto py-6 header-text" style="font-size: 3.75rem;">{!! translatableContent('about', 'about-title') !!}</h1> --}}
                    <div class="grid lg:grid-cols-3">
                        <div class="col-span-2">
                            <div class="px-10 lg:px-0">
                                <h1 class="text-white text-center md:text-left text-5xl lg:text-6xl font-medium mx-auto mb-4">{!! $page->title !!}</h1>
                                @if($page->subtitle !== null)
                                    <p class="text-white text-center md:text-left">{!! $page->subtitle !!}</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="hero-fallback-image-container absolute w-full h-full hidden top-0 left-0" style="z-index: -1;">
                <div class="center-cover-bg bg-lazy-load h-full block lg:hidden mb-8 lg:mb-0" data-style="{{ backgroundCSSImage('selling.hero') }}"></div>
            </div>

            <div class="hidden hero-video-container">
                <div class="absolute w-full h-full top-0 left-0" style="z-index: -10;">
                    <video autoplay="" loop="" muted="" poster="" class="w-full h-full object-cover object-center" style="transform: scale(2);">
                        <source src="/themes/bramleys/assets/videos/selling-with-bramleys.mp4" type="video/mp4" />
                    </video>
                </div>
            </div>

            <div class="absolute top-0 left-0 w-full h-full opacity-50 bg-black" style="z-index: -10;"></div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.banners.property-search-banner'))

    <section class="bg-whiter">
        <div class="container mx-auto py-18 px-2 lg:py-20">
            <div class="bg-whiter">
                <div class="container mx-auto px-6 md:px-0">
                    <div class="grid grid-cols-1 lg:grid-cols-2 gap-16">
                        <div>
                            <h3 class="font-medium pb-6 text-5xl text-center md:text-left header-text">{!! $page->title !!}</h3>
                            <p class="text-center md:text-left text-base">{!! $page->content !!}</p>
                        </div>
                        <div>
                            <img class="w-full" src="{{ assetPath($page->image) }}" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container mx-auto py-18 lg:py-32 lg:px-12">
            <div class="grid grid-cols-1 lg:grid-cols-2 lg:gap-16 items-center">
                <div class="col-span-1 px-6 lg:px-0 mb-8 lg:mb-0">
                    <div class="lg:pr-16 center-cover-bg bg-lazy-load h-80 sm:h-114 md:h-140 lg:h-108" data-style="{{ backgroundCSSImage('selling.section-2') }}"></div>
                </div>

                <div class="col-span-1 px-10 lg:px-0">
                    <h1 class="text-6xl header-text mb-8">Award winning service</h1>
                    <p>Thank you to all of our clients who took the time to write about their experience with Bramleys. As a result of your kind words, four of our well estbalished offices won two gold awards per office for the best sales agent in our areas in the 2021 allAgents awards. All of our staff are trained to industry standards to ensure we provide professional, unbiased advice together with unrivalled, first class customer service. Our clients will always remain the heart of our business and we endeavour to give you the best experience in your sale.</p>

                    @php
                        $awards = [
                            [
                                'image' => themeImage('awards/WF16.png'),
                            ],
                            [
                                'image' => themeImage('awards/Wf14.png'),
                            ],
                            [
                                'image' => themeImage('awards/Mirf.png'),
                            ],
                            [
                                'image' => themeImage('awards/HX5.png'),
                            ],
                            [
                                'image' => themeImage('awards/HUdds.png'),
                            ],
                            [
                                'image' => themeImage('awards/HUdds.png'),
                            ],
                            [
                                'image' => themeImage('awards/Heckmond.png'),
                            ],
                            [
                                'image' => themeImage('awards/HD1.png'),
                            ]
                        ]
                    @endphp

                    <div class="relative pt-8 border-t mt-12">
                        <div class="mySwiperAwards overflow-hidden px-4 overflow-hidden">
                            <div class="swiper-wrapper w-full">
                                @foreach ($awards as $award)
                                    <div class="swiper-slide text-center px-4">
                                        <div class="w-full">
                                            <img class="h-auto w-full" src="{{ $award['image'] }}" alt="star-outline" loading="lazy">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>

                        <div class="absolute inset-y-1/2 w-full -translate-y-1/2 flex justify-between lg:block z-40">
                            <div class="swiper-button-prevBtn-awards relative md:absolute md:left-0 md:top-2 cursor-pointer inline-block pr-2 md:pr-0">
                                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-gray-400 fill-current stroke-current h-8" alt="arrow" loading="lazy" style="transform: rotate(180deg);">
                            </div>
                            <div class="swiper-button-nextBtn-awards relative md:absolute md:right-0 md:top-2 cursor-pointer inline-block">
                                <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-gray-400 fill-current stroke-current h-8" alt="arrow" loading="lazy">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.why-choose-bramleys-slider'))

    <section class="primary-bg">
        <div class="center-cover-bg bg-lazy-load w-full h-full py-16" data-style="{{ backgroundCSSImage('home.testimonial') }}">
            <div class="container mx-auto px-8 lg:p-28 justify-center">
                <div class="text-center text-white lg:px-40">
                    <h1 class="text-6xl header-text text-center mb-6">Download our selling guide</h1>
                    <p class="mb-16">Download our sellers guide below to help with form ipsum dolor sedit consectur dolor sedit lorem dolor sedit</p>

                    <a class="border text-white px-8 py-4 rounded" href="">Download guide</a>
                </div>
            </div>
        </div>
    </section>

    @php
        $items = [
            [
                'title' => 'Market Appraisal',
                'text' => 'Book your free, no obligation appraisal. We will arrange for one of our qualified valuers to meet you at your home',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Inspection',
                'text' => 'After looking around your home, we will sit down with you and discuss the best way to market your property, assessing recent comparable evidence and taking into account current market conditions.',
                'ordered' => true,
                'additionalClasses' => [
                    'container' => 'lg:-mt-24',
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Marketing',
                'text' => 'Bramleys utilise the latest technology to best show your home to prospective buyers.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Premium packages',
                'text' => 'We may recommend upgrades to premium advertising – view our Premium Package page',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Ready to sell',
                'text' => 'Once you are ready to sell, we will arrange a marketing appointment with you to take the photographs and produce the floorplans to create your brochure. Before the property goes live, you will be sent a copy of the particulars for your approval.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Contracts and ID',
                'text' => 'We require two forms of identification for each legal owner together with our signed contract. We will collect this from you at the marketing appointment.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Property launch',
                'text' => 'The listing will go live on all property portals and will be shared on social media. One of our dedicated negotiators will contact you to introduce themselves and will be your main point of contact throughout the process.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Viewings and offers',
                'text' => 'Viewings will take place resulting in offers. We will discuss these with you direct, advising you the situation of each potential buyer, gathering proof of funding and checking chains so that you can make a decision based on your circumstance.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Instructing solicitors',
                'text' => 'Once you have chosen a solicitor, we will instruct them on your behalf',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Sales progression',
                'text' => 'Your dedicated negotiator will guide you all the way through the sales process and will work with you to overcome any unforeseen obstacles that may come in the way. By speaking with you and your buyer, we ensure everything is proceeding as it should, and where a chain is involved, we will speak with other parties involved to make sure everyone is at a similar stage.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ],
            [
                'title' => 'Exchange and completion',
                'text' => 'Once the financial and legal elements are completed, you should be ready to discuss dates with your buyer. We will guide you through this process to successful completion.',
                'ordered' => true,
                'additionalClasses' => [
                    'text' => 'text-center lg:text-left',
                ]
            ]
        ]
    @endphp

    <section id="start-your-property-journey" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('selling.journey') }}">
        <div class="p-10 pt-12 lg:py-32 lg:pt-16 container mx-auto">
            <div class="lg:px-68 md:w-3/5 mx-auto">
                <h1 class="text-5xl lg:text-6xl header-text text-center mb-16 mx-auto">Start your property journey with Bramleys</h1>
            </div>

            @include(themeViewPath('frontend.components.timeline'), ['items' => $items])
        </div>
    </section>

    <section id="enquiry-form">
        <div class="container px-8 py-20 lg:p-32 mx-auto">
            <h1 class="text-6xl header-text text-center mb-4">Enquire about selling with us</h1>
            <p class="text-center mb-10">Interested in selling your property? Start with a property valuation</p>

            <div class="shadow rounded p-8 md:p-16">
                @include(themeViewPath('frontend.forms.contact-form'), ['items' => $items, 'buttonLabel' => trans('contact.get_started')])
            </div>
        </div>
    </section>

    <section id="mortgage">
        <div class="grid grid-cols-1 lg:grid-cols-2">
            <div class="center-cover-bg bg-lazy-load min-h-92" data-style="{{ backgroundCSSImage('selling.mortgage') }}"></div>

            <div class="text-white p-20 lg:pr-20 xl:pr-56" style="background: #333;">
                <p class="uppercase font-bold secondary-text mb-6 text-sm">BUYING WITH BRAMLEYS</p>
                <h1 class="mb-8 text-6xl header-text xl:w-4/5">Need help finding your next home?</h1>
                <p class="mb-12">Speak to your valuer or negotiator about your specific requirements and they can keep you updated of any properties that may suit you, sometimes before they come to market!</p>

                <div>
                    <a class="primary-bg rounded text-white px-8 py-4" href="{{ localeUrl('/contact') }}">Get in touch</a>
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.footer'))
@endsection
