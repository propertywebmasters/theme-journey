@extends('layouts.app')

@section('content')

    @php
    $backgroundImageStyle = $page->hero_background === null
        ? backgroundCSSImage('about.hero')
        : 'background: linear-gradient(rgba(0, 0, 0, 0.5), rgba(0, 0, 0, 0.5)), url("'.assetPath($page->hero_background).'");'
    @endphp

    {{-- site header component, include's navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <section id="about" class="center-cover-bg bg-lazy-load" data-style="{{ $backgroundImageStyle }}">
        <div class="py-64 px-4 sm:px-10 lg:px-0">
            <div class="container mx-auto transparent text-left">
                <div class="mb-6 md:mb-16">
                    <h1 class="text-white text-center md:text-left text-5xl lg:text-6xl font-medium mx-auto mb-4">{!! $page->title !!}</h1>
                    @if($page->subtitle !== null)
                        <p class="text-white text-center md:text-left">{!! $page->subtitle !!}</p>
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.banners.property-search-banner'))

    <div class="pt-12 lg:pt-20 pb-12 bg-whiter">
        <div class="container mx-auto px-6 md:px-0">

            <div class="grid grid-cols-1 lg:grid-cols-2 gap-16">
                <div>
                    <h3 class="font-medium pb-6 text-5xl text-left header-text text-center md:text-left">{!! $page->title !!}</h3>
                    <p class="text-center md:text-left text-base">{!! $page->content !!}</p>
                </div>
                <div>
                    <img class="w-full" src="{{ assetPath($page->image) }}" />
                </div>
            </div>
        </div>
    </div>

    @include(themeViewPath('frontend.components.why-choose-bramleys-slider'))

    <div class="pt-12 lg:pt-20 pb-20 bg-whiter">
        <div class="container mx-auto px-4 md:px-0">

            <div class="grid grid-cols-1 md:grid-cols-2 gap-16">
                <div>
                    <img class="w-full" src="{{ assetPath(translatableContent('about', 'about-main-image')) }}" alt="image">
                </div>
                <div>
                    <h3 class="font-medium pb-6 text-5xl text-center md:text-left header-text">{!! translatableContent('about', 'about-section-1-title') !!}</h3>
                    <p class="text-center md:text-left text-base">{!! translatableContent('about', 'about-section-1-text') !!}</p>
                </div>
            </div>
        </div>
    </div>

    @include(themeViewPath('frontend.components.latest-news'))

    @include(themeViewPath('frontend.components.testimonials'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
