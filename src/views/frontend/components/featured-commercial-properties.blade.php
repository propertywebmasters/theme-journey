@php
    $i = 0;
    $saleProperties = $properties->where('tender_type', 'sale');
    $rentalProperties = $properties->where('tender_type', 'rental');

    $isMultiMode = $saleProperties->count() > 0 && $rentalProperties->count() > 0;
@endphp

@if ($properties->count() > 0)
    @php
        $header = trans('header.similar_properties');
        if (isset($customHeader)) {
            $header = $customHeader;
        }

        if (!isset($bgColor)) {
            $bgColor = 'bg-white';
        }

        if (!isset($card)) {
            $card = 'default';
        }
    @endphp

    <!-- ===================== Featured Properties ============================== -->
    <section class="py-8 pb-12 md:py-16 md:pb-24 {{ $bgColor }}">
        <div class="container px-4 sm:px-10 md:px-4 mx-auto relative">
            <h3 class="text-3xl text-center md:pb-8 pb-4">{{ $header }}</h3>
            <div class="mx-auto text-center md:mx-0 md:text-left pb-4 md:pb-0">
                <div class="swiper-button-prevBtn relative md:absolute md:right-9 md:top-2 cursor-pointer inline-block pr-2 md:pr-0">
                    <img src="{{ themeImage('property/sarrow-left.svg') }}" class="svg-inject secondary-text" alt="arrow" loading="lazy">
                </div>
                <div class="swiper-button-nextBtn relative md:absolute md:top-2 md:right-2 cursor-pointer inline-block">
                    <img src="{{ themeImage('property/sarrow-right.svg') }}" class="svg-inject secondary-text" alt="arrow" loading="lazy">
                </div>
            </div>

            <div class="s-slider swiper mySwiperCommercialProperties overflow-hidden md:p-0 z-1 list-none">
                <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">
                    @foreach($properties as $property)
                        @include(themeViewPath("frontend.components.cards.featured-commercial-property"))
                    @endforeach
                </div>
            </div>
        </div>
    </section>


@endif

{{-- @php
    $i = 0;
@endphp
@if ($properties->count() > 0)
    @php
    $header = trans('header.similar_properties');
    if (isset($customHeader)) {
        $header = $customHeader;
    }
    @endphp

    <section id="featured-properties">
        <div class="container py-4 lg:px-4 xl:px-0 pt-12 pb-16 mx-auto">
            <div class="items-baseline pb-6 text-center lg:text-left">
                <h2 class="text-2xl font-normal header-text">{{ $header }}</h2>
            </div>
            <div class="grid grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach($properties as $property)
                    @if($i > 3) @break @endif
                    @php
                    if ($i == 3) {
                        $extraCss = 'hidden md:block lg:hidden';
                    } else {
                        $extraCss = '';
                    }
                    @endphp

                    @include(themeViewPath('frontend.components.cards.property'), ['property' => $property, 'extraCss' => $extraCss])
                    @php $i++ @endphp
                @endforeach
            </div>
        </div>
    </section>


@endif --}}
