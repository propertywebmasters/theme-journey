<section id="valuation" class="relative min-h-146">
    <div class="lg:absolute grid grid-cols-1 lg:grid-cols-2 w-full">
        <div class="center-cover-bg bg-lazy-load min-h-146 w-full" data-style="{{ backgroundCSSImage('home.valuation') }}"></div>

        <div class="tertiary-bg h-full w-full hidden lg:block">&nbsp;</div>
    </div>

    <div class="grid grid-cols-1 lg:grid-cols-2 container mx-auto lg:absolute left-1/2 top-1/2 transform lg:-translate-x-1/2 lg:-translate-y-1/2">
        <div class="hidden lg:block">&nbsp;</div>

        <div class="text-white py-18 p-8 lg:py-0 lg:pl-10 lg:pr-8 xl:pr-0">
            <p class="uppercase font-bold text-sm text-white mb-6">{{ translatableContent('home', 'get-valuation-subtitle') }}</p>
            <h1 class="mb-6 lg:mb-8 header-text" style="font-size: 3.2rem; line-height: 1;">{{ translatableContent('home', 'get-valuation-title') }}</h1>
            <p class="mb-12 font-light">{!! translatableContent('home', 'get-valuation-content') !!}</p>

            <div class="w-full lg:w-11/12">
                <form action="{{ localeUrl('/valuation') }}" method="GET">
                    <div class="grid grid-cols-5 md:grid-cols-3">
                        <div class="col-span-3 md:col-span-2">
                            <input class="bg-white w-full h-full py-4 px-6 text-black focus:outline-none rounded-tl rounded-bl text-sm" type="text" name="postcode" placeholder="{{ trans('generic.your_postcode') }}">
                        </div>
                        <div class="col-span-2 md:col-span-1">
                            <button type="submit" class="rounded-tr rounded-br cta text-white py-4 w-full">{{ trans('button.get_valuation') }}</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>