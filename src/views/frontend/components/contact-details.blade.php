<div id="contact-details" class="bg-white flex flex-row lg:flex-col xl:flex-row items-center {{ empty(assetPath($contactDetails['profile_picture'])) ? 'justify-center' : '' }} p-5 shadow mt-10 text-left">
    <div class="w-28 h-28 mr-5 lg:mr-0 xl:mr-5 mb-0 lg:mb-3 xl:mb-0 {{ empty(assetPath($contactDetails['profile_picture'])) ? 'hidden' : '' }}">
        <img class="w-full h-full rounded-full object-cover object-center" src="{{ assetPath($contactDetails['profile_picture']) }}" alt="">
    </div>

    <div>
        <p class="text-sm mb-3">{{ trans('contact.this_property_is_marketed_by') }}</p>
        <p class="font-bold text-sm" style="color: var(--ap-cta-bg);">{{ $contactDetails['name'] }}</p>
        <p>
            <a class="text-sm" href="mailto:{{ $contactDetails['email'] }}?subject={{ 'Enquiry for '.$property->displayName().' - ('.$property->displayReference().')' }}">{{ $contactDetails['email'] }}</a>
        </p>
        <p>
            <a class="text-sm" href="tel:{{ $contactDetails['tel'] }}">{{ $contactDetails['tel'] }}</a>
        </p>
    </div>
</div>