<section>
    <div class="container mx-auto px-4 py-20 lg:py-28 lg:px-32">
        <div class="text-center lg:px-32">
            <div class="mb-16">
                <h1 class="text-5xl lg:text-6xl header-text mb-6">From the past to the present meet our team</h1>
                <p>See the experienced staff behind Bramleys who help us to ensure we remain market leaders</p>
            </div>

            <div class="text-center">
                <a href="{{ localeUrl('/meet-the-team') }}" class="primary-bg text-white rounded px-8 py-4">{{ trans('header.meet_the_team') }}</a>
            </div>
        </div>
    </div>
</section>
