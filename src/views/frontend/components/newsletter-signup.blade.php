<div class="sm:col-span-2 col-span-1 text-center sm:text-right md:pr-8 lg:pr-0">

    <form id="newsletter-signup" class="inline-block w-full" method="post" action="javascript:;" enctype="application/x-www-form-urlencoded">
        <div class="flex w-full">
            <div class="h-12 bg-white rounded-tl rounded-bl py-3" style="flex-grow: 1;">
                <input class="w-full block bg-transparent focus:outline-none h-full pr-6 text-sm pl-4" type="text" name="newsletter_name"
                       placeholder="{{ trans('contact.full_name') }}" style="border-right-width: 1px;">
            </div>

            <div class="h-12 bg-white py-3" style="flex-grow: 1;">
                <input class="w-full block bg-transparent focus:outline-none h-full pr-6 text-sm pl-4" type="email" name="newsletter_email"
                       placeholder="{{ trans('placeholder.enter_email_address') }}">
            </div>

            <div class="h-12" style="flex-grow: 1;">
                <button class="w-full primary-bg text-white px-4 sm:px-6 lg:px-12 py-3 rounded-tr rounded-br h-full" type="submit">
                    {{ trans('button.submit') }}
                </button>
            </div>
        </div>
        @csrf
    </form>
</div>
