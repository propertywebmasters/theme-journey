<section class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('home.why-bramleys') }}">
    <div class="text-center container mx-auto px-16 py-18 lg:py-32">
        <h3 class="mb-16 text-3xl font-medium">Why choose Bramleys?</h3>

        <div class="grid grid-cols-1 lg:grid-cols-3 gap-8 lg:gap-16">
            <div>
                <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                    <div class="absolute top-1/2 left-1/2" style="transform: translate(-50%, -50%) scale(1.25);">
                        <img class="svg-inject h-36 w-auto fill-current secondary-text mx-auto" src="{{ themeImage('icons/roof.svg') }}" alt="roof" loading="lazy">
                    </div>
                </div>
                <h4 class="text-2xl mb-4">Under one roof</h4>
                <p>Our full range of property services can be specifically tailored to suit your needs.</p>
            </div>

            <div>
                <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                    <div class="absolute top-1/2 left-1/2" style="transform: translate(-50%, -50%) scale(1.25);">
                        <img class="svg-inject h-36 w-auto fill-current secondary-text mx-auto" src="{{ themeImage('icons/qualified.svg') }}" alt="qualified" loading="lazy">
                    </div>
                </div>
                <h4 class="text-2xl mb-4">Fully qualified</h4>
                <p>As a Chartered Surveying practice, we are regulated by the RICS as well as all of our residential valuers being Propertymark protected.</p>
            </div>

            <div>
                <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                    <div class="absolute top-1/2 left-1/2" style="transform: translate(-50%, -50%) scale(1.0125);">
                        <img class="svg-inject h-36 w-auto fill-current secondary-text mx-auto" src="{{ themeImage('icons/people.svg') }}" alt="people" loading="lazy">
                    </div>

                </div>
                <h4 class="text-2xl mb-4">Established office network</h4>
                <p>With offices in Huddersfield, Halifax, Elland, Mirfield and Heckmondwike.</p>
            </div>

            <div>
                <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                    <div class="p-3">
                        <img class="svg-inject h-full w-auto fill-current secondary-text mx-auto" src="{{ themeImage('icons/tech.svg') }}" alt="tech" loading="lazy">
                    </div>
                </div>
                <h4 class="text-2xl mb-4">Investment in the latest technology</h4>
                <p>Offering innovative technologies such as 360 virtual tours, professional photography and aerial shots as part of our premium packages</p>
            </div>

            <div>
                <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                    <div class="p-3">
                        <img class="svg-inject h-full w-auto fill-current secondary-text mx-auto" src="{{ themeImage('icons/star-outline.svg') }}" alt="star-outline" loading="lazy">
                    </div>
                </div>
                <h4 class="text-2xl mb-4">Commitment to excellence</h4>
                <p>We are proud of our enviable reputation that can be reflected in our award winning reviews.</p>
            </div>

            <div>
                <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                    <div class="p-3">
                        <img class="svg-inject h-full w-auto fill-current secondary-text mx-auto" src="{{ themeImage('icons/map.svg') }}" alt="map" loading="lazy">
                    </div>
                </div>
                <h4 class="text-2xl mb-4">Independent, partner led</h4>
                <p>Being one of the largest, independent property service firms in West Yorkshire that is headed up by four Partners</p>
            </div>
        </div>
    </div>

    @php
    $showSearch = true;
    if(isset($includeSearchBar) && $includeSearchBar === false) {
        $showSearch = false;
    }
    @endphp
    @if($showSearch)
    <div class="primary-bg text-center text-white py-8">
        <a class="text-xl relative" href="{{ localeUrl('/all-properties-for-sale') }}">
            Begin your <span class="header-text text-4xl">Property Search</span> here

            <div class="absolute -right-36 -top-14 hidden lg:block">
                <img class="svg-inject h-20 w-auto fill-current secondary-text" src="{{ themeImage('swoop.svg') }}" alt="swoop" loading="lazy">
            </div>
        </a>
    </div>
    @endif
</section>
