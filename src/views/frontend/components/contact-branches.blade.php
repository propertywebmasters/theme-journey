@if ($branches->count() > 0)

    <section id="contact-branches" class="py-10">
        <div class="text-center mx-auto">
            <h3 class="text-2xl text-center header-text mb-12">{{ trans('generic.our_branches') }}</h3>
        </div>

        @foreach($branches as $branch)
            @include(themeViewPath('frontend.components.cards.branch'))
        @endforeach
    </section>
@endif
