@php
    $searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals();
    $hasStudentRentals = isset($searchControls->student_rentals) && $searchControls->student_rentals;
    $hasHolidayRentals = isset($searchControls->holiday_rentals) && $searchControls->holiday_rentals;
@endphp

<section class="pt-24 pb-6 tertiary-bg">
    <div class="container mx-auto px-4 lg:px-0">

        @include(themeViewPath('frontend.components.full-screen-filter'))
        <form action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">
            <div class="w-full flex items-center justify-center flex-wrap lg:flex-nowrap">
                <div class="lg:w-44 w-full lg:mb-0 mb-3 text-center lg:text-right pr-4">
                    <span class="text-lg text-white tracking-tightest">{{ trans('search.start_your_search') }}</span>
                </div>
                <div class="flex lg:w-180 w-full content-between flex-wrap lg:flex-nowrap">

                    @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                        <div class="bg-white px-4 py-3 rounded-l-0 lg:rounded-l lg:w-36 w-full rounded lg:rounded-r-none my-2 lg:my-0" style="flex-grow: 1;">
                            <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent text-sm appearance-none select-carat">
                                <option value="sale" @if ($searchRequest->get('type') === 'sale') selected @endif>{{ trans('label.sale') }}</option>
                                <option value="rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === null) selected @endif>{{ trans('label.rental') }}</option>
                                @if($hasShortTermRentals)
                                    <option value="short_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }} {{ trans('properties') }}</option>
                                @endif
                                @if($hasLongTermRentals)
                                    <option value="long_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                                @endif
                                @if($hasStudentRentals)
                                    <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                                @endif
                                @if($hasHolidayRentals)
                                    <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                                @endif
                            </select>
                        </div>
                    @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                        @if($searchControls->tender_type === 'sale_only')
                            <input id="search_type" name="type" type="hidden" value="sale">
                        @elseif($searchControls->tender_type === 'rental_only')
                            <div class="bg-white px-4 py-3 rounded-l-0 lg:rounded-l lg:w-36 w-full rounded lg:rounded-r-none my-2 lg:my-0" style="flex-grow: 1;">
                                <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent  text-sm appearance-none select-carat">
                                    <option value="rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === null) selected @endif>{{ trans('label.rental') }}</option>
                                    @if($hasShortTermRentals)
                                        <option value="short_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_SHORT) selected @endif>{{ trans('label.short_term_rental') }}</option>
                                    @endif
                                    @if($hasLongTermRentals)
                                        <option value="long_term_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('rental_length') === \App\Models\Property::RENTAL_LENGTH_LONG) selected @endif>{{ trans('label.long_term_rental') }}</option>
                                    @endif
                                    @if($hasStudentRentals)
                                        <option value="student_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_student_rental')) selected @endif>{{ trans('label.student_rental') }}</option>
                                    @endif
                                    @if($hasHolidayRentals)
                                        <option value="holiday_rental" @if ($searchRequest->get('type') === 'rental' && $searchRequest->get('is_holiday_rental')) selected @endif>{{ trans('label.holiday_rental') }}</option>
                                    @endif
                                </select>
                            </div>
                        @endif
                    @endif


                    @php
                        $rounded = '';
                        $border = '';

                        if (isset($searchControls->tender_type) && $searchControls->tender_type !== 'sale_only') {
                            $rounded = 'lg:rounded-l-none';
                            $border = 'lg:border-l';
                        }
                    @endphp

                    <div class="bg-white px-4 py-3 lg:w-104 w-full rounded {{ $rounded }} lg:rounded-r-none my-2 lg:my-0" style="flex-grow: 1;">
                        @include(themeViewPath('frontend.components.location-search'), [
                            'additionalClasses' => '-ml-1 secondary-text lg:pl-2 border-l-0 pl-0',
                            'refinedSearch' => true,
                            'value' => $searchRequest->get('location'),
                            'border' => $border,
                        ])
                    </div>

                    <div class=" bg-white px-4 py-3 lg:rounded-r rounded-r-0 text-sm lg:w-40 w-full rounded lg:rounded-l-none my-2 lg:my-0" style="flex-grow: 1;">
                        <span class="full-screen-filter-toggle tracking-tight lg:border-l lg:pl-2 border-l-0 cursor-pointer block text-sm">
                                <img src="{{ themeImage('filter.svg') }}" class="svg-inject h-4 inline-block pr-1" loading="lazy"> {{ trans('label.filters') }}
                            </span>
                    </div>
                </div>

                <div class="lg:w-36 w-full mt-2 lg:mt-0 lg:ml-2">
                    <input type="submit"
                           class="py-3 lg:py-2 block text-lg text-white w-full h-full focus:outline-none cursor-pointer hover:bg-hover hover:text-white hover:border-hover duration-500 rounded cta"
                           value="Search">
                </div>
            </div>

            @if($searchRequest->get('is_development'))
                <input type="hidden" name="is_development" value="1">
            @endif
        </form>
    </div>
</section>

{{-- Filters Popup --}}
<section class="relative">

    <div class="bg-primary py-24 absolute w-full left-0 top-0 z-0 opacity-0 invisible transition-all duration-500" id="popUp">
        <div class=" cursor-pointer right-4 absolute top-8" id="closePop">
            <img src="assets/img/close.svg" alt="close">
        </div>
        <div class="container mx-auto px-4 ">

            <div class="grid lg:grid-cols-5 xl:gap-x-28 gap-x-8 gap-y-8">
                <div class="col-span-3">
                    <div class="grid-cols-2 grid gap-x-5 gap-y-5">
                        <select class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3" name="" id="">
                            <option value="0">Min Price</option>
                            <option value="1">Min Price</option>
                        </select>
                        <select class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3" name="" id="">
                            <option value="0">Bathrooms</option>
                            <option value="1">Bathrooms</option>
                        </select>
                        <select class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3" name="" id="">
                            <option value="0">Max Price</option>
                            <option value="1">Max Price</option>
                        </select>
                        <select class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3" name="" id="">
                            <option value="0">Min Price</option>
                            <option value="1">Min Price</option>
                        </select>
                        <select class="col-span-2 border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-2" name="" id="">
                            <option value="0">Min Beds</option>
                            <option value="1">Min Beds</option>
                        </select>
                    </div>

                </div>
                <div class="col-span-2">
                    <h3 class="text-2xl font-medium text-white">Property Types</h3>
                    <div class="mt-8 grid grid-cols-2">
                        <div class="flex items-center mr-4 mb-4">
                            <input id="radio1" type="radio" name="radio" class="hidden" checked="">
                            <label for="radio1" class="flex items-center cursor-pointer tracking-tight text-white">
                                <span class="w-4 h-4 inline-block mr-2 rounded-full border-white border-2 flex-no-shrink"></span>
                                Penthouse</label>
                        </div>

                        <div class="flex items-center mr-4 mb-4">
                            <input id="radio2" type="radio" name="radio" class="hidden">
                            <label for="radio2" class="flex items-center cursor-pointer tracking-tight text-white">
                                <span class="w-4 h-4 inline-block mr-2 rounded-full border-white border-2 flex-no-shrink"></span>
                                Apartment</label>
                        </div>

                        <div class="flex items-center mr-4 mb-4">
                            <input id="radio3" type="radio" name="radio" class="hidden">
                            <label for="radio3" class="flex items-center cursor-pointer tracking-tight text-white">
                                <span class="w-4 h-4 inline-block mr-2 rounded-full border-white border-2 flex-no-shrink"></span>
                                Villa</label>
                        </div>
                        <div class="flex items-center mr-4 mb-4">
                            <input id="radio4" type="radio" name="radio" class="hidden" checked="">
                            <label for="radio4" class="flex items-center cursor-pointer tracking-tight text-white">
                                <span class="w-4 h-4 inline-block mr-2 rounded-full border-white border-2 flex-no-shrink"></span>Commercial</label>
                        </div>

                        <div class="flex items-center mr-4 mb-4">
                            <input id="radio5" type="radio" name="radio" class="hidden">
                            <label for="radio5" class="flex items-center cursor-pointer tracking-tight text-white">
                                <span class="w-4 h-4 inline-block mr-2 rounded-full border-white border-2 flex-no-shrink"></span>
                                Land</label>
                        </div>

                        <div class="flex items-center mr-4 mb-4">
                            <input id="radio6" type="radio" name="radio" class="hidden">
                            <label for="radio6" class="flex items-center cursor-pointer tracking-tight text-white">
                                <span class="w-4 h-4 inline-block mr-2 rounded-full border-white border-2 flex-no-shrink"></span>
                                House</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flex gap-x-2 mt-8">
                <a class="text-sm text-center tracking-wide text-browngrey rounded-3xl border border-browngrey max-w-xs block  py-3 px-8 transition-all hover:bg-browngrey hover:text-white duration-500"
                   href="#">Reset</a>
                <a class="text-sm text-center tracking-wide nav-text  rounded-3xl border border-activeCcolor max-w-xs block  py-3 px-8 transition-all cta hover:bg-hover hover:text-white hover:border-hover duration-500"
                   href="#">Update</a>
            </div>
        </div>
    </div>
</section>
