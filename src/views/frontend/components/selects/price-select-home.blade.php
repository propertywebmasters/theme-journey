@php
$classes = 'form-control w-full focus:outline-none rounded-none appearance-none select-carat lg:border-l lg:pl-4';

if (!isset($additionalClasses)) {
    $additionalClasses = '';
}
@endphp
<select id="{{ $id }}" name="{{ $name }}" class="{{ $classes }} {{ $additionalClasses }}">
    <option value="">{{ $initialOption }}</option>
    @foreach($prices as $price)
        @php
            $priceFormat = $defaultCurrency->symbol.number_format($price);
            if($mode === 'rental') {
                $priceFormat .= ' '.trans('rental_frequency.'.$defaultRentalFrequency);
            }
            $selected = $currentValue != 0 && $price == $currentValue ? ' selected="selected"' : ''
        @endphp
        <option value="{{ $price }}"{{ $selected }}>{!! $priceFormat !!}</option>
    @endforeach
</select>
