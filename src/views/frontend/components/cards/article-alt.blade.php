@if (isset($full_width) && $full_width)
    <div>
        <a href="{{ localeUrl('/news/'.$article->url_key) }}" title="{{ trans('generic.view_this_article') }}">
            <img class="w-full h-60 sm:h-84 md:h-118 lg:h-80 object-cover object-center" src="{{ assetPath($article->image) }}" alt="news" loading="lazy">
        </a>
    </div>
    <div class="pl-0 pr-4 xl:pr-8 py-8 flex items-center">
        <div>
            <span class="text-base leading-loose tracking-wide uppercase text-gray-500">
                {{ $article->created_at->formatLocalized('%d %b %Y') }}
            </span>
            <div class="mb-2 mt-4">
                <a href="{{ localeUrl('/news/'.$article->url_key) }}" title="{{ trans('generic.view_this_article') }}">
                    <h3 class="text-2xl line-clamp-2">
                        {{ $article->title }}
                    </h3>
                </a>
            </div>

            <div class="border-t pt-6">
                <p class="text-base leading-normal tracking-tight font-light line-clamp-2 text-gray-500">
                    {{ Str::limit(strip_tags($article->content, 360)) }}
                </p>
            </div>
            <a class="view-article text-sm text-center tracking-wide rounded border inline-block py-3 px-10 xl:mt-8 mt-4 transition-all primary-bg text-white duration-500 hover-cta-bg" href="{{ localeUrl('/news/'.$article->url_key) }}">{{ trans('generic.view_this_article') }}</a>
        </div>
    </div>
@else
    <div class="border mb-8">
        <div class="">
            <a href="{{ localeUrl('/news/'.$article->url_key) }}" title="{{ trans('generic.view_this_article') }}">
                <img class="w-full h-60 sm:h-84 md:h-118 lg:h-80 object-cover object-center" src="{{ assetPath($article->image) }}" alt="news">
            </a>
        </div>
        <div class="xl:pt-6 xl:pb-10 xl:px-10 px-4 py-4">
            <div>
                <span class="text-base leading-loose tracking-wide uppercase text-gray-500">
                    {{ $article->created_at->formatLocalized('%d %b %Y') }}
                </span>
                <a href="{{ localeUrl('/news/'.$article->url_key) }}" class="block text-2xl mb-2 mt-4 line-clamp-2">
                    <h3>{{ Str::limit(strip_tags($article->content, 360)) }}</h3>
                </a>

                <div class="border-t pt-6">
                    <p class="text-base leading-normal tracking-tight font-light line-clamp-2 text-gray-500">
                        {{ Str::limit(strip_tags($article->content, 360)) }}
                    </p>
                </div>

                <a class="view-article block md:hidden text-sm text-center tracking-wide rounded-3xl border inline-block py-3 px-10 xl:mt-8 mt-4 transition-all primary-bg text-white duration-500" href="{{ localeUrl('/news/'.$article->url_key) }}">{{ trans('generic.view_this_article') }}</a>

            </div>
        </div>
    </div>
@endif
