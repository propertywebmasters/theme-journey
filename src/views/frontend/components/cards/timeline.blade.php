@php
    $date = isset($left) && $left ? 'primary-bg -left-4' : 'secondary-bg -right-4';

    $text = isset($left) && $left ? '' : 'ml-auto';

    if (isset($item['date']) && isset($left) && $left) {
        $text .= ' pl-28';
    } 

    if (isset($item['date']) && !isset($left)) {
        $text .= ' pr-28';
    }

    $image = isset($left) && $left ? 'lg:left-24' : 'lg:-left-24';

    $additionalClasses = [
        'text' => '',
        'image' => '',
        'date' => '',
        'container' => '',
    ];

    if (isset($item['additionalClasses'])) {
        foreach ($item['additionalClasses'] as $key => $value) {
            $additionalClasses[$key] = $value;
        }
    }
@endphp

<div class="timeline-content-container relative {{ $additionalClasses['container'] }} @if (isset($item['image'])) max-h-56 h-56 @endif {{ isset($left) && $left ? 'left' : 'right' }} timeline-item-{{ $i }}">
    <div class="timeline-circle relative z-30 hidden lg:block">
        @if (isset($item['ordered']) && $item['ordered'])
            <div class="flex items-center justify-center h-full">
                <span class="timeline-order-number">{{ ++$i }}</span>
            </div>
        @endif
    </div>

    @if (isset($item['date']))
        <div class="{{ $date }} text-white absolute py-2 px-6 top-8 z-30  {{ $additionalClasses['date'] }}">
            <h1 class="text-4xl header-text font-normal">{{ $item['date'] }}</h1>
        </div>
    @endif

    <div class="p-8 @if (!isset($item['image'])) pb-10 @endif w-full lg:w-2/3 border shadow timeline-content h-full bg-white rounded relative z-20 {{ $text }} {{ $additionalClasses['text'] }}">
        @if (isset($item['title']))
            <h3 class="text-xl mb-4">{{ $item['title'] }}</h3>
        @endif

        <p class="text-sm">{{ $item['text'] }}</p>

        @if (isset($item['image']))
            <div class="absolute rounded top-40 w-full rounded {{ $image }} {{ $additionalClasses['image'] }}">
                <img class="w-11/12 h-56 object-cover rounded-xl" src="{{ assetPath($item['image']) }}" />
            </div>
        @endif
    </div>
</div>
