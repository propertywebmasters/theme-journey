<div class="swiper-slide flex-shrink-0 relative">
    <img src="{{ themeImage('team/quote.png') }}" alt="quote" class="md:absolute left-0 top-0 mb-4">

    <div class="text-left md:pl-24 mt-4 pr-4">
        <p style="font-size: 1.71rem;" class="leading-tight tracking-tight line-clamp-4 mb-4">{!! $testimonial->content !!}</p>
        <span class="text-lg tracking-tight text-white mt-3 inline-block">{{ $testimonial->company_name ? $testimonial->name . ', ' . $testimonial->company_name : $testimonial->name }}</span>
    </div>
</div>
