@php
    $imageClasses = 'min-h-50 max-h-50 sm:min-h-56 sm:max-h-56 md:min-h-60 md:max-h-60 lg:min-h-50 lg:max-h-50 xl:min-h-50 xl:max-h-50'
@endphp

<div class="w-full bg-white shadow-md @if (isset($css) && $css !== null) {{ $css }} @endif">

    <div onclick="window.location='{{ localeUrl('/news/'.$article->url_key) }}'" class="{{ $imageClasses }} overflow-hidden center-cover-bg bg-lazy-load cursor-pointer" data-style="background-image: url('{{ assetPath($article->image) }}'); background-position: center center;">
        <!-- -->
    </div>
    <div class="px-4 py-4 lg:px-4 xl:px-8 xl:py-8">
        <div class="py-0 pb-3 md:min-h-50 md:h-50 lg:min-h-56 lg:h-56 xl:min-h-40 xl:h-40 sm:overflow-hidden">
            <a href="{{ localeUrl('/news/'.$article->url_key) }}" class="block text-xl font-medium hover:underline leading-5 mb-2" title="{{ $article->title }}">{{ $article->title }}</a>
            <p class="text-gray-400 text-sm pb-4 font-light">{{ $article->created_at->format('jS F Y') }}</p>
            <p class="text-sm font-light">{!!  \Illuminate\Support\Str::limit(strip_tags($article->content), 200)  !!}</p>
        </div>
    </div>
</div>
