<div class="mb-8 lg:mb-0 sm:col-span-1">
    <div class="h-80 lg:h-92 mb-4 relative overlay-trigger">
        <img class="h-full w-full object-cover" src="{{ assetPath($teamMember->photo) }}" />

        <div class="overlay primary-bg-transparent absolute top-0 left-0 w-full h-full">
            <div class="flex items-center justify-center h-full">
                <a class="mr-4" href="tel:{{ $teamMember->tel }}">
                    <img src="{{ themeImage('icons/social/phone.svg') }}" class="svg-inject fill-current text-white h-8" alt="phone" loading="lazy">
                </a>

                <a class="" href="mailto:{{ $teamMember->email }}">
                    <img src="{{ themeImage('icons/social/email.svg') }}" class="svg-inject fill-current text-white h-8" alt="email" loading="lazy">
                </a>
            </div>
        </div>
    </div>

    <div>
        <p class="text-xl">{{ $teamMember->name }}</p>
        <p class="primary-text">{{ $teamMember->role }}</p>
    </div>
</div>