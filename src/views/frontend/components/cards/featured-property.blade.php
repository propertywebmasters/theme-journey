@php
    use Illuminate\Support\Str;$splitMapMode = $splitMapMode ?? false;
    $images = $property->images->take(3);
    $swiperId = Str::random();

    if (!isset($additionalClasses)) {
        $additionalClasses = '';
    }
@endphp

<div class="swiper-slide flex-shrink-0 relative list-single">
    <div class="mySwiperPropertiesInnerContainer overlay-trigger">
        <a href="{{ localeUrl('/property/'.$property->url_key) }}">
            <div
                class="s-slider swiper mySwiperPropertiesInner overflow-hidden md:p-0 z-1 list-none relative z-10 max-w-5xl">
                <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">
                    @foreach($images as $image)
                        <div class="swiper-slide flex-shrink-0 relative">
                            <img class="w-full h-80 sm:h-60 md:h-72 lg:h-80 {{ $additionalClasses }} object-cover" src="{{ getPropertyImage($image->filepath, 'lg') }}" alt="img">
                            {{-- <div class="bg-opacity-40 bg-black absolute top-0 left-0 w-full h-full z-0 transition-all duration-500"></div> --}}
                        </div>
                    @endforeach
                </div>

                {{-- <div class="flex items-center absolute top-4 right-4 z-30">
                    <div class="cursor-pointer flex">
                        <img class="svg-inject swiper-button-nextInner text-white mr-3" src="{{ themeImage('property/sarrow-left.svg') }}" alt="prev" loading="lazy">
                        <img class="svg-inject swiper-button-prevInner text-white" src="{{ themeImage('property/sarrow-right.svg') }}" alt="next" loading="lazy">
                    </div>
                </div> --}}

                <div class="overlay"></div>
            </div>

            <div
                class="mySwiperPropertiesInnerDetails absolute left-0 xl:px-8 px-4 w-full z-10  transition-all hovered-fade duration-500">
                <p class="text-2xl text-white block line-clamp-1">{!! $property->displayName() !!}</p>
                <span class="text-sm tracking-tight text-white">{{ $property->location->displayAddress() }}</span>
                <p class="text-xl tracking-tight font-medium text-white md:mt-7 mt-2 mb-4">{!! $property->displayPrice() !!}</p>
                @include(themeViewPath('frontend.components.property.bed-bath-info'), ['additionalClasses' => 'text-white'])
            </div>
        </a>

        
    </div>

    @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
        <div class="absolute top-4 right-4 z-30">
            @if (user() === null)
                @php
                    $actionClass = 'modal-button';
                    $dataAttribs = 'data-target="preauth-modal"';
                    $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif
            <div class="cursor-pointer">
                <a href="javascript:" class="{{ $actionClass }}" {!! $dataAttribs !!}><img loading="lazy" class="svg-inject text-white {{ $imgClass }}" src="{{ themeImage('property/heart.svg') }}" alt="heart" title="{{ trans('shortlist.save') }}"></a>
            </div>
        </div>
    @endif
</div>
