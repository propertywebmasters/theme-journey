@php
    $splitMapMode = $splitMapMode ?? false;

    $class = $data = '';
    if (!isset($extraCss)) {
        $extraCss = '';
    }
    if ($splitMapMode) {
        $class = 'map-listing cursor-pointer';
        $data =  'data-zoom="12" data-latitude="'.$property->location->latitude.'" data-longitude="'.$property->location->longitude.'"';
    } else {
        $data =  'data-gallery_images="'.propertyImagesString($property).'"';
    }
@endphp


<div class="relative rounded-lg property-listing inline-listing-gallery z-1 {{ $class }} {{ $extraCss }}" {{ $data }}>
    <div class="relative min-h-76 h-76 max-h-76 overflow-hidden">

        @if($property->tender_status !== \App\Models\Property::TENDER_STATUS_AVAILABLE)
            <div class="absolute tender-status top-2 right-2 primary-bg text-white p-2 rounded-l-2xl rounded-r-2xl">{{ $property->tender_status }}</div>
        @endif

        @php
            $imageUrl = $property->images->first() === null ? '/img/image-coming-soon.jpg' : $property->images->first()->filepath;
            $bgImageClass = 'min-height: 300px; max-height: 300px; overflow-hidden background-position: center center; background-size: cover; width: 100%; cursor: pointer; background-image: url("'.getPropertyImage($imageUrl).'")';
        @endphp

        <div class="rounded-t-lg bg-lazy-load" style="{{ $bgImageClass }}" data-style="{{ $bgImageClass }}" id="property-{{ $property->uuid }}" onclick="window.location='{{ localeUrl('/property/'.$property->url_key) }}'">&nbsp;</div>
        @if(hasFeature(\App\Models\TenantFeature::FEATURE_LISTINGS_INLINE_GALLERY) && !$splitMapMode && $property->images->count() > 1)
            <div class="inline-gallery-control hidden previous-image p-4 absolute left-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/caret-right.svg') }}" loading="lazy" style="transform: rotate(180deg);">
            </div>
            <div class="inline-gallery-control hidden next-image p-4 absolute right-0 top-1/2 -mt-8 text-white cursor-pointer" data-target="property-{{ $property->uuid }}">
                <img class="inline-block svg-inject text-white fill-current stroke-current h-6 p-0 m-0" src="{{ themeImage('icons/caret-right.svg') }}" loading="lazy">
            </div>
        @endif
    </div>
    <div class="relative px-5 lg:px-7 md:py-7 pt-4 pb-7 shadow-md rounded-lg">

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
            @if (user() === null)
                @php
                    $actionClass = 'modal-button';
                    $dataAttribs = 'data-target="preauth-modal"';
                    $imgClass = '';
                @endphp
            @else
                @php
                    if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray())) {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = 'primary-text fill-current stroke-current';
                    } else {
                        // this property is in the shortlist
                        $actionClass = 'shortlist-toggle-simple';
                        $dataAttribs = 'data-on-class="primary-text fill-current stroke-current" data-property="'.$property->uuid.'"';
                        $imgClass = '';
                    }
                @endphp
            @endif
        @endif

        <div class="pb-6">
            <a href="{{ localeUrl('/property/'.$property->url_key) }}" class="block text-md font-medium tracking-wide hover:underline h-10 leading-5 overflow-hidden line-clamp-2" title="{{ $property->displayName() }}">{!! $property->displayName() !!}</a>
            <p class="location-name text-sm tracking-wide font-light line-clamp-1" title="{{ $property->location->displayAddress() }}">{{ $property->location->displayAddress() }}</p>
            <span class="text-2xl font-bold py-2 primary-text block">{!! $property->displayPrice(); !!}</span>
            @include(themeViewPath('frontend.components.property.bed-bath-info'))
        </div>

        <div class="flex justify-content-end pt-6 border-t-2">
            @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHORTLIST_SYSTEM))
                <div class="flex-1 text-left text-base font-bold tracking-wider items-baseline">
                    @if (user() === null)
                        <a href="javascript:" class="reg hover:underline primary-text modal-button text-uppercase" title="{{ trans('shortlist.save') }}" data-on-class="text-red-500" data-off-class="primary-text" data-target="preauth-modal" data-on-text="{{ trans('shortlist.saved') }}" data-off-text="{{ trans('shortlist.save') }}" data-off-symbol="&#9734;" data-on-symbol="&#9733;" data-property="{{ $property->uuid }}">
                            <img class="svg-inject inline-block primary-text stroke-current fill-current h-6 -mt-1" src="{{ themeImage('icons/star.svg') }}"> <span class="save-cta uppercase">{{ trans('shortlist.save') }}</span>
                        </a>
                    @else
                        @if (in_array($property->uuid, $shortlistedProperties->pluck('property_uuid')->toArray()))
                            <a href="javascript:" class="shortlist-toggle text-red-500 text-uppercase" title="{{ trans('shortlist.saved') }}" data-on-class="text-red-500" data-off-class="primary-text" data-on-text="{{ trans('shortlist.saved') }}" data-off-text="{{ trans('shortlist.save') }}" data-off-symbol="&#9734;" data-on-symbol="&#9733;" data-property="{{ $property->uuid }}">
                                <img class="svg-inject inline-block text-red stroke-current fill-current h-6 -mt-1" src="{{ themeImage('icons/star.svg') }}"> <span class="save-cta uppercase">{{ trans('shortlist.saved') }}</span>
                            </a>
                        @else
                            <a href="javascript:" class="shortlist-toggle primary-text text-uppercase" title="{{ trans('shortlist.saved') }} " data-on-class="text-red-500" data-off-class="primary-text" data-on-text="{{ trans('shortlist.saved') }}" data-off-text="{{ trans('shortlist.save') }}" data-off-symbol="&#9734;" data-on-symbol="&#9733;" data-property="{{ $property->uuid }}">
                                <img class="svg-inject inline-block primary-text stroke-current fill-current h-6 -mt-1" src="{{ themeImage('icons/star.svg') }}"> <span class="save-cta uppercase">{{ trans('shortlist.save') }}</span>
                            </a>
                        @endif
                    @endif
                </div>
            @endif
            <div class="flex-1 text-right tracking-wider items-baseline">
                <a class="inline-block text-base text-cta font-bold uppercase" href="{{ localeUrl('/property/'.$property->url_key) }}" title="View this property">{!! trans('generic.view') !!} <img class="svg-inject inline-block text-cta stroke-current fill-current h-4 pl-1 -mt-0.5" src="{{ themeImage('icons/right-arrow.svg') }}"></a>
            </div>
        </div>
    </div>
</div>
