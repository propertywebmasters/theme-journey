@php
    if (!isset($customHeader)) {
        $customHeader = trans('header.latest_news');
    }
@endphp

@if ($articles->count() > 0)
    @php
        $i = 0
    @endphp

    <section id="latest-news" class="mb-20 py-10 px-4 sm:px-40 md:px-10 lg:p-0">
        <div class="container mx-auto bg-white md:pt-12 pt-7 px-8 xl:px-0">
            <div class="flex flex-col sm:flex-row justify-between pb-7 sm:border-b sm:mb-12 items-center relative">
                <div class="w-full mb-4 sm:mb-0">
                    <h3 class="text-2xl font-medium header-text text-center">{{ $customHeader }}</h3>
                </div>

                <div class="sm:absolute right-0 top-0">
                    <a class="border rounded px-4 py-2 block text-sm font-medium text-slate-400" href="{{ localeUrl('/news') }}">
                        {{ trans('button.view_all_news') }}
                    </a>
                </div>
            </div>
            <div class="grid md:grid-cols-2 lg:grid-cols-3 gap-8">
                @foreach($articles as $article)
                    @php $i++ @endphp
                    @include(themeViewPath('frontend.components.cards.article'), ['article' => $article, 'css' => $i == 4 ? 'hidden md:block lg:hidden' : ''])
                @endforeach
            </div>
        </div>
    </section>
@endif
