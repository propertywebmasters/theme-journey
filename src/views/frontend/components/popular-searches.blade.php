@if ($popularSearches->count())
    <section id="our-top-picks">
        <div class="container mx-auto pt-16 pb-32 px-8 xl:px-0">
            <div class="pb-7 mb-12 text-center border-b">
                <h3 class="text-2xl font-medium">{{ translatableContent('home', 'our-top-picks-title') }}</h3>
            </div>

            <div class="grid lg:grid-cols-2 gap-8">
                @foreach ($popularSearches as $search)
                    <div class="col-span-1 center-cover-bg bg-lazy-load relative h-100 md:h-118 lg:h-84 xl:h-92 overlay-trigger" data-style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ assetPath($search->image) }}')">
                        <div class="absolute bottom-16 w-full text-center text-white z-30 our-top-picks-text">
                            <p class="text-2xl font-medium">{!! $search->title !!}</p>
                        </div>

                        <div class="absolute z-30 bottom-0 our-top-picks-button left-1/2 transform -translate-x-1/2">
                            <a class="border rounded text-white py-2 px-4 lg:px-6 font-medium block hover:bg-white hover:text-black ease-in" href="{{ $search->url }}">
                                {{ trans('generic.view') }}
                            </a>
                        </div>

                        <div class="overlay"></div>
                    </div>
                @endforeach

                {{-- <div class="col-span-1 center-cover-bg bg-lazy-load relative h-100 md:h-118 lg:h-84 xl:h-92 overlay-trigger" data-style="{{ backgroundCSSImage('home.development') }}">
                    <div class="absolute bottom-16 w-full text-center text-white z-30 our-top-picks-text">
                        <p class="text-2xl font-medium">Our top pick development</p>
                    </div>

                    <div class="absolute z-30 bottom-0 our-top-picks-button left-1/2 transform -translate-x-1/2">
                        <a class="border rounded text-white py-2 px-4 lg:px-6 font-medium block hover:bg-white hover:text-black ease-in" href="">View development</a>
                    </div>

                    <div class="overlay"></div>
                </div>

                <div class="col-span-1 center-cover-bg bg-lazy-load relative h-100 md:h-118 lg:h-84 xl:h-92 overlay-trigger" data-style="{{ backgroundCSSImage('home.auction') }}">
                    <div class="absolute bottom-16 w-full text-center text-white z-30 our-top-picks-text">
                        <p class="text-2xl font-medium">View our latest auction lots</p>
                    </div>

                    <div class="absolute z-30 bottom-0 our-top-picks-button left-1/2 transform -translate-x-1/2">
                        <a class="border rounded text-white py-2 px-4 lg:px-6 font-medium block hover:bg-white hover:text-black" href="">View development</a>
                    </div>

                    <div class="overlay"></div>
                </div> --}}
            </div>
        </div>
    </section>
@endif