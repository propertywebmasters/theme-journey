<!--  Breadcrumb -->
@php
    $additionalClasses = '';

    if (isset($class)) {
        $additionalClasses = $class;
    }
@endphp

@if (isset($navigation) && is_array($navigation))
    @php
    $i = 1;
    $count = count($navigation)
    @endphp
    <nav class="{{ $additionalClasses }}">
        <ol class="flex items-center gap-x-2 text-xs">
            @foreach($navigation as $nav)
                @php
                    $keys = array_keys($nav);
                    $values = array_values($nav);
                    $anchor = $keys[0];
                    $url = $values[0]
                @endphp

                @if ($i !== $count)
                    <li class="text-xs"><a href="{{ $url }}" class="text-xs underline duration-300 primary-text">{!! $anchor !!}</a></li>
                @else
                    <li class="text-xs">{!! $anchor !!}</li>
                @endif

                @if ($i !== $count)
                    <li class="text-xs">&gt;</li>
                @endif
                @php $i++ @endphp
            @endforeach
        </ol>
    </nav>
@endif
