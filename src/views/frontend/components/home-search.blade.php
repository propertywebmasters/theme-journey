@php
    use App\Models\Property;use App\Models\TenantFeature;$searchControls = searchControlOptions(config('platform-cache.search_controls_enabled'));
    $absolutelyPosition = $absolutelyPosition ?? false;
    $primaryGridCols = 9;
    $secondaryGridCols = 7;

    // decrement grid col counter if we are not including the tender type control
    // primary search

    if(isset($searchControls->tender_type) && $searchControls->tender_type === 'sale_only') {
        --$primaryGridCols;
    }
    if(!isset($searchControls->location) || !$searchControls->location) {
        $primaryGridCols -= 2;
    }

    if(!isset($searchControls->property_type) || !$searchControls->property_type) {
        $primaryGridCols -= 2;
    }

    if(!isset($searchControls->min_price) || !$searchControls->min_price) {
        --$primaryGridCols;
    }
    if(!isset($searchControls->max_price) || !$searchControls->max_price) {
        --$primaryGridCols;
    }

    // if(!isset($searchControls->property_type) || !$searchControls->property_type) {
    //     --$primaryGridCols;
    // }

    $bedroomFilterMode = isset($searchControls->bedrooms) && $searchControls->bedrooms ? 'single' : null;
    $bedroomFilterMode = isset($searchControls->bedrooms_as_range) && $searchControls->bedrooms_as_range ? 'multi' : $bedroomFilterMode;

    if ($bedroomFilterMode === null) {
         $secondaryGridCols = ($secondaryGridCols - 2);
    } elseif ($bedroomFilterMode === 'single') {
        --$secondaryGridCols;
    } elseif($bedroomFilterMode === 'multi') {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->bathrooms) || !$searchControls->bathrooms) {
        --$secondaryGridCols;
    }

    if(!isset($searchControls->reference) || !$searchControls->reference) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->name) || !$searchControls->name) {
        --$secondaryGridCols;
    }
    if(!isset($searchControls->status) || !$searchControls->status) {
        --$secondaryGridCols;
    }

    // DO NOT REMOVE
    // md:grid-cols-8 md:grid-cols-7 md:grid-cols-6 md:grid-cols-5
    // lg:grid-cols-8 lg:grid-cols-7 lg:grid-cols-6 lg:grid-cols-5

    $hasShortTermRentals = hasShortTermRentals();
    $hasLongTermRentals = hasLongTermRentals()
@endphp

<div @if($absolutelyPosition) class="w-full absolute top-1/3 lg:top-1/2 z-30 sm:px-0" @else class="sm:px-0" @endif>
    <form id="home-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded"
          @if($absolutelyPosition) class="mx-auto w-4/5 xl:w-3/5" @endif>
        <div class="grid grid-cols-2 lg:grid-cols-{{ $primaryGridCols }} w-full relative">

            @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                <div class="bg-white p-4 col-span-2 lg:col-span-1 rounded lg:rounded-tl lg:rounded-bl lg:rounded-tr-none lg:rounded-br-none mb-2 lg:mb-0">
                    {{-- <label for="search_type" class="text-xs">{{ trans('label.for') }}</label> --}}
                    <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent appearance-none select-carat">
                        <option value="sale">{{ trans('label.sale') }}</option>
                        <option value="rental">{{ trans('label.rental') }}</option>
                        @if($hasShortTermRentals)
                            <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                        @endif
                        @if($hasLongTermRentals)
                            <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                        @endif
                    </select>
                </div>
            @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                @if($searchControls->tender_type === 'sale_only')
                    <input id="search_type" name="type" type="hidden" value="sale">
                @elseif($searchControls->tender_type === 'rental_only')
                    <div class="bg-white p-4 col-span-2 lg:col-span-1 rounded lg:rounded-bl mb-2 lg:mb-0 lg:rounded-tr-none lg:rounded-br-none">
                        {{-- <label for="search_type" class="text-xs">{{ trans('label.for') }}</label> --}}
                        <select id="search_type" name="type" class="w-full block focus:outline-none -ml-1 bg-transparent appearance-none select-carat">
                            <option value="rental">{{ trans('label.rental') }}</option>
                            @if($hasShortTermRentals)
                                <option value="short_term_rental">{{ trans('label.short_term_rental') }}</option>
                            @endif
                            @if($hasLongTermRentals)
                                <option value="long_term_rental">{{ trans('label.long_term_rental') }}</option>
                            @endif
                        </select>
                    </div>
                @endif
            @endif

            @if(isset($searchControls->location) && $searchControls->location)
                <div class="col-span-2 bg-white p-4 relative rounded lg:rounded-none mb-2 lg:mb-0">
                    {{-- <label for="search_location" class="text-xs">{{ trans('label.where') }}</label> --}}
                    <div class="flex items-center lg:border-l lg:pl-4">
                        <label for="search_location">{{ trans('label.location') }}</label>
                        <input id="search_location" type="text" name="location" placeholder="{{ trans('placeholder.area_town_postcode') }}"
                               class="autocomplete-location block w-full focus:outline-none pl-4 bg-transparent"
                               autocomplete="off" value="{{ $location ?? '' }}" data-search-results-container="desktop-search-results-container"/>
                    </div>

                    <div id="autocomplete-results" class="click-outside absolute top-0 w-full mt-14 -ml-4 z-20" style="z-index: 100;"
                         data-classes="py-1 px-4 border bg-white hover:bg-gray-200 cursor-pointer">
                        <ul class="desktop-search-results-container"></ul>
                    </div>
                    <input type="hidden" name="location_url" value="">
                </div>
            @endif

            @if(isset($searchControls->property_type) && $searchControls->property_type)
                <div class="col-span-2 bg-white p-4 rounded lg:rounded-none mb-2 lg:mb-0">
                    <select id="property_type" name="property_type"
                            class="form-control w-full focus:outline-none rounded-none appearance-none select-carat lg:border-l lg:pl-4 bg-transparent">
                        <option value="">{{ trans('placeholder.property_type') }}</option>
                        @foreach($propertyTypeCategories as $category)
                            <option value="{{ $category['category_slug'] }}">{{ transPropertyType($category['category']) }}</option>
                        @endforeach
                    </select>
                </div>
            @endif

            @if(isset($searchControls->min_price) && $searchControls->min_price)
                <div class="sale_price_filter col-span-1 bg-white p-4 lg:py-4 lg:px-0 rounded-tl rounded-bl lg:rounded-none">
                    @include(themeViewPath('frontend.components.selects.price-select-home'), [
                            'mode' => 'sale',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'sale_min_price',
                            'name' => 'sale_min_price',
                            'prices' => $searchPriceRanges['sale'],
                            'initialOption' => trans('label.min_price'),
                            'currentValue' => null,
                            'additionalClasses' => 'lg:bg-center-90 bg-transparent',
                        ])
                </div>
            @endif
            @if(isset($searchControls->max_price) && $searchControls->max_price)
                <div class="sale_price_filter col-span-1 bg-white p-4 lg:py-4 lg:px-0 rounded-tr rounded-br lg:rounded-none">
                    @include(themeViewPath('frontend.components.selects.price-select-home'), [
                            'mode' => 'sale',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'sale_max_price',
                            'name' => 'sale_max_price',
                            'prices' => $searchPriceRanges['sale'],
                            'initialOption' => trans('label.max_price'),
                            'currentValue' => null,
                            'additionalClasses' => 'lg:bg-center-90 bg-transparent',
                        ])
                </div>
            @endif

            @if(isset($searchControls->min_price) && $searchControls->min_price)
                <div class="rental_price_filter hidden">
                    @include(themeViewPath('frontend.components.selects.price-select-home'), [
                            'mode' => 'rental',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'rental_min_price',
                            'name' => 'rental_min_price',
                            'prices' => $searchPriceRanges['rental'],
                            'initialOption' => trans('label.min_price'),
                            'currentValue' => null,
                            'additionalClasses' => 'lg:bg-center-90 bg-transparent',
                        ])
                </div>
            @endif

            @if(isset($searchControls->max_price) && $searchControls->max_price)
                <div class="rental_price_filter hidden">
                    @include(themeViewPath('frontend.components.selects.price-select-home'), [
                            'mode' => 'rental',
                            'defaultRentalFrequency' => $defaultRentalFrequency,
                            'defaultCurrency' => $defaultCurrency,
                            'id' => 'rental_max_price',
                            'name' => 'rental_max_price',
                            'prices' => $searchPriceRanges['rental'],
                            'initialOption' => trans('label.max_price'),
                            'currentValue' => null,
                            'additionalClasses' => 'lg:bg-center-90 bg-transparent',
                        ])
                </div>
            @endif

            <div class="content-center z-20 mt-4 lg:mt-0">
                <button type="button"
                        role="button" id="home-advanced-search-button"
                        class="block text-white w-full h-full cursor-pointer p-4 xl:p-4 focus:outline-none primary-bg hover-lighten tracking-wider rounded-tr rounded-br rounded-tl rounded-bl lg:rounded-tl-none lg:rounded-bl-none"
                        onclick="return false;">
                    <div class="flex items-center justify-center">
                        <span class="mr-4" style="letter-spacing: 0.125px;">{{ trans('label.advanced') }}</span>
                        <span>
                            <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject text-white h-4 fill-current stroke-current" alt="arrow" loading="lazy"
                                 style="transform: rotate(90deg);">
                        </span>
                    </div>
                </button>
            </div>
            <div class="z-20 ml-2 mt-4 lg:mt-0">
                <button type="submit" class="block text-white w-full h-full focus:outline-none cursor-pointer secondary-bg hover-lighten tracking-wider rounded"
                        style="letter-spacing: 0.125px;">
                    {{ trans('label.start_search') }}
                </button>
            </div>

            <div id="home-advanced-search"
                 class="z-10 absolute top-80 lg:top-14 w-1/2 lg:w-full lg:top-14 grid grid-cols-1 lg:grid-cols-{{ $secondaryGridCols }} gap-4 text-white p-4 pt-6 md:pt-4 primary-bg-transparent w-100 invisible mx-auto"
                 style="background-color: var(--ap-primary-transparent); width: fit-content;">

                {{-- @if(isset($searchControls->property_type) && $searchControls->property_type)
                    <div>
                        <select id="property_type" name="property_type"
                                class="form-control w-full py-0 md:py-3 focus:outline-none text-gray-300 border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 tertiary-bg text-sm appearance-none select-carat">
                            <option value="">{{ trans('placeholder.property_type') }}</option>
                            @foreach($propertyTypeCategories as $category)
                                <option value="{{ $category['category_slug'] }}">{{ transPropertyType($category['category']) }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif --}}

                @if($bedroomFilterMode !== null)
                    @if ($bedroomFilterMode === 'single')
                        <div>
                            <select id="bedrooms" name="bedrooms"
                                    class="form-control w-full py-0 md:py-3 focus:outline-none border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 bg-transparent text-white text-sm appearance-none select-carat">
                                <option value="">{{ trans('placeholder.bedrooms') }}</option>
                                @foreach($bedroomsRange as $bedroomCount)
                                    <option value="{{ $bedroomCount }}">{{ trans('label.min') }} {{ $bedroomCount }}+</option>
                                @endforeach
                            </select>
                        </div>
                    @elseif($bedroomFilterMode === 'multi')
                        <div>
                            <select id="bedrooms" name="min_bedrooms"
                                    class="form-control w-full py-0 md:py-3 focus:outline-none border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 bg-transparent text-white text-sm appearance-none select-carat">
                                <option value="">{{ trans('label.min').' '.trans('placeholder.bedrooms') }}</option>
                                @foreach($bedroomsRange as $bedroomCount)
                                    <option value="{{ $bedroomCount }}">{{ trans('label.min') }} {{ $bedroomCount }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <select id="bedrooms" name="max_bedrooms"
                                    class="form-control w-full py-0 md:py-3 focus:outline-none border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 bg-transparent text-white text-sm appearance-none select-carat">
                                <option value="">{{ trans('label.max').' '.trans('placeholder.bedrooms') }}</option>
                                @foreach($bedroomsRange as $bedroomCount)
                                    <option value="{{ $bedroomCount }}">{{ trans('label.max') }} {{ $bedroomCount }}</option>
                                @endforeach
                            </select>
                        </div>
                    @endif
                @endif

                @if(isset($searchControls->bathrooms) && $searchControls->bathrooms)
                    <div>
                        <select id="bathrooms" name="bathrooms"
                                class="form-control w-full py-0 md:py-3 focus:outline-none border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 bg-transparent text-white text-sm appearance-none select-carat">
                            <option value="">{{ trans('placeholder.bathrooms') }}</option>
                            @foreach($bathroomsRange as $bathroomCount)
                                <option value="{{ $bathroomCount }}">{{ trans('label.min') }} {{ $bathroomCount }}+</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if(isset($searchControls->status) && $searchControls->status)
                    <div>
                        <select id="status" name="status"
                                class="form-control w-full py-0 md:py-3 focus:outline-none border-gray-300 rounded-none border-b border-t-0 border-l-0 border-right-0 bg-transparent text-white text-sm appearance-none select-carat">
                            <option value="">Status</option>
                            @foreach(Property::searchableTenderStatusList() as $tenderStatus)
                                <option value="{{ $tenderStatus }}">{{ trans('property_status.'.strtolower($tenderStatus)) }}</option>
                            @endforeach
                        </select>
                    </div>
                @endif

                @if(isset($searchControls->reference) && $searchControls->reference)
                    <div>
                        <input name="reference" class="form-control w-full py-0 md:py-3 px-2 focus:outline-none border-b border-gray-300 bg-transparent text-white text-sm"
                               placeholder="{{ trans('label.reference') }}">
                    </div>
                @endif

                @if(isset($searchControls->name) && $searchControls->name)
                    <div>
                        <input name="name"
                               class="form-control w-full py-0 md:py-3 px-2 focus:outline-none border-b border-gray-300 bg-transparent text-white text-sm placeholder:text-white"
                               placeholder="{{ trans('label.property_name') }}">
                    </div>
                @endif

                <input type="hidden" name="locale" value="{{ app()->getLocale() }}">
                @if(hasFeature(TenantFeature::FEATURE_DEVELOPMENT_SEARCH_ON_HOMEPAGE))
                    <input type="hidden" name="is_development" value="1">
                @endif

            </div>

        </div>
    </form>
</div>
