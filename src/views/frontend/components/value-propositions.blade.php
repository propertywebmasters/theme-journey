@if (hasFeature(\App\Models\TenantFeature::FEATURE_VALUE_PROPOSITIONS))
    <section class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('home.value-propositions') }}">
        <div class="text-center container mx-auto px-16 py-18 lg:py-32">
            <h3 class="mb-16 text-3xl font-medium">{{ translatableContent('home', 'value-proposition-title') }}</h3>

            <div class="grid grid-cols-1 lg:grid-cols-3 gap-8 lg:gap-16">
                @foreach ($valuePropositions as $valueProposition)
                    <div>
                        @if ($valueProposition->image)
                            <div class="w-40 h-40 mb-6 primary-bg rounded-full mx-auto relative">
                                <div class="absolute top-1/2 left-1/2" style="transform: translate(-50%, -50%) scale(1.25);">
                                    <img class="svg-inject h-36 w-auto fill-current secondary-text mx-auto" src="{{ assetPath($valueProposition->image) }}" alt="roof" loading="lazy">
                                </div>
                            </div>
                        @endif
                        <h4 class="text-2xl mb-4">{{ $valueProposition->title }}</h4>
                        <p>{{ $valueProposition->content }}</p>
                    </div>
                @endforeach
            </div>
        </div>

        @php
        $showSearch = true;
        if(isset($includeSearchBar) && $includeSearchBar === false) {
            $showSearch = false;
        }
        @endphp
        @if($showSearch)
        <div class="primary-bg text-center text-white py-8">
            <a class="text-xl relative" href="{{ localeUrl('/all-properties-for-sale') }}">
                Begin your <span class="header-text text-4xl">Property Search</span> here

                <div class="absolute -right-36 -top-14 hidden lg:block">
                    <img class="svg-inject h-20 w-auto fill-current secondary-text" src="{{ themeImage('swoop.svg') }}" alt="swoop" loading="lazy">
                </div>
            </a>
        </div>
        @endif
    </section>

@endif