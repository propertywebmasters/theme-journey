@php
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $useSettingsCache = config('platform-cache.site_settings_enabled');

    $siteLogo = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], $useSettingsCache));
    $footerLogo = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_FOOTER_LOGO['key'], $useSettingsCache));
    $logoPath = !empty($footerLogo) ? $footerLogo : $siteLogo
@endphp
<footer>
    <!-- FOOTER -->
    <section id="footer" class="footer-section pt-16">
        <div class="container mx-auto px-8 lg:px-0">
            @include(themeViewPath('frontend.components.accreditations'))
        </div>

        <div class="pb-12">
            <div class="container mx-auto px-8 xl:px-0">
                <div class="grid grid-cols-1 md:grid-cols-4">
                    <div class="col-span-2 mb-4 text-center lg:text-left mx-auto sm:mx-0">
                        <h1 class="text-3xl header-text mb-2 mx-auto w-3/5 lg:w-full">{{ trans('header.sign_up_to_our_mailing_list_today') }}</h1>
                        <p class="text-sm w-9/12 lg:w-full mx-auto footer-link">{{ trans('mailing-list.keep_up_to_date') }}</p>
                    </div>
                    @if(hasFeature(\App\Models\TenantFeature::FEATURE_MAILING_LIST))
                        @include(themeViewPath('frontend.components.newsletter-signup'))
                    @endif
                </div>
            </div>
        </div>

        <div class="container mx-auto px-8 xl:px-0">
            <div class="grid grid-cols-1 md:grid-cols-{{$footerBlocks->count()}} pt-12 text-center sm:text-left border-b pb-12">
                @foreach($footerBlocks as $block)
                    <div class="mb-8 sm:mb-0">
                        <span class="pt-2 xl:pb-1 block text-lg font-medium tracking-tight primary-text font-medium text-lg footer-text">{!! localeStringFromCollection($block, null, config('platform-cache.footers_enabled')) !!}</span>
                        <ul class="text-sm leading-6 font-extralight">
                            @foreach ($block->navigation as $link)
                                <li><a href="{{ localeUrl($link->url) }}" target="{{ $link->target }}" class="text-sm font-light hover:underline {{ $link->class }}"
                                       title="View {{ localeStringFromCollection($link, null, config('platform-cache.footers_enabled')) }}">{!! localeStringFromCollection($link, null, config('platform-cache.footers_enabled'))  !!}</a></li>
                            @endforeach
                        </ul>
                    </div>
                @endforeach
            </div>
        </div>
    </section>

    {{-- @include(themeViewPath('frontend.components.accreditations')) --}}
    @include(themeViewPath('frontend.components.social-footer'))
    {{-- @include(themeViewPath('frontend.components.super-footer')) --}}
</footer>
