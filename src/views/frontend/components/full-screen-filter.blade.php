@php
    $currentCurrency = currentCurrency();
    $saleSelected = true;
    $rentalSelected = false;
    if (!isset($searchRequest)) {
        $searchRequest = new \App\Http\Requests\SearchRequest();
    }
    if ($searchRequest->request->has('type') && $searchRequest->request->get('type') === 'rental') {
        $saleSelected = false;
        $rentalSelected = true;
    }

    $classes = 'h-6 text-white absolute top-4 right-4 md:right-10 md:top-6';
    $location = $searchRequest->request->get('location') ?? '';
    $locationUrl = $searchRequest->request->get('location_url') ?? '';

@endphp

<div id="home-advanced-search" class="full-screen-filter secondary-bg md:pt-2 xl:py-36 fixed w-full left-0 top-0 invisible transition-all duration-500 min-h-screen h-screen" style="z-index: 9999999999; overflow: scroll;">
    <form id="advanced-search" action="{{ localeUrl('/search') }}" method="post" enctype="application/x-www-form-urlencoded">
        <div class="container filter-container mx-auto px-4" style="overflow: scroll;">

            <div class="flex justify-between lg:pb-6 pb-4 border-b border-grey2 border-opacity-70 xl:mb-12 lg:mb-10 mb-8">
                <h3 class="text-2xl md:text-4xl font-medium text-white pt-4">{{ trans('label.advanced') }} {{ trans('label.search') }}</h3>
                <div class="close-full-screen-filter cursor-pointer text-white">
                    <img src="{{ themeImage('close.svg') }}" alt="close" class="{{ $classes }}">
                </div>
            </div>

            <div class="grid lg:grid-cols-5 gap-x-16">
                <div class="col-span-3">
                    <div class="grid-cols-2 grid gap-x-5 gap-y-5 pt-2">

                        @if(isset($searchControls->tender_type) && $searchControls->tender_type === 'both')
                            <div class="col-span-2">
                                <select id="search_type" name="type" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                    <option value="sale">{{ trans('label.sale') }} {{ trans('generic.properties') }}</option>
                                    <option value="rental">{{ trans('label.rental') }} {{ trans('generic.properties') }}</option>
                                    @if($hasShortTermRentals)
                                        <option value="short_term_rental">{{ trans('label.short_term_rental') }} {{ trans('generic.properties') }}</option>
                                    @endif
                                    @if($hasLongTermRentals)
                                        <option value="long_term_rental">{{ trans('label.long_term_rental') }}  {{ trans('generic.properties') }}</option>
                                    @endif
                                    @if($hasStudentRentals)
                                        <option value="student_rental">{{ trans('label.student_rental') }}  {{ trans('generic.properties') }}</option>
                                    @endif
                                    @if($hasHolidayRentals)
                                        <option value="holiday_rental">{{ trans('label.holiday_rental') }}  {{ trans('generic.properties') }}</option>
                                    @endif
                                </select>
                            </div>
                        @elseif(isset($searchControls->tender_type) && ($searchControls->tender_type === 'sale_only' || $searchControls->tender_type === 'rental_only'))
                            @if($searchControls->tender_type === 'sale_only')
                                <input id="search_type" name="type" type="hidden" value="sale">
                            @elseif($searchControls->tender_type === 'rental_only')
                                <div class="col-span-2">
                                    <select id="search_type" name="type" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                        <option value="rental">{{ trans('label.rental') }} {{ trans('generic.properties') }}</option>
                                        @if($hasShortTermRentals)
                                            <option value="short_term_rental">{{ trans('label.short_term_rental') }} {{ trans('generic.properties') }}</option>
                                        @endif
                                        @if($hasLongTermRentals)
                                            <option value="long_term_rental">{{ trans('label.long_term_rental') }} {{ trans('generic.properties') }}</option>
                                        @endif
                                        @if($hasStudentRentals)
                                            <option value="student_rental">{{ trans('label.student_rental') }} {{ trans('generic.properties') }}</option>
                                        @endif
                                        @if($hasHolidayRentals)
                                            <option value="holiday_rental">{{ trans('label.holiday_rental') }} {{ trans('generic.properties') }}</option>
                                        @endif
                                    </select>
                                </div>
                            @endif
                        @endif

                        @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_LOCATION_DATALIST))
                            @include(themeViewPath('frontend.components.location-datalist'))
                        @else
                            <div class="col-span-2 h-full">
                                <div class="relative w-full">
                                    <div id="autocomplete-results" class="click-outside absolute top-12 z-30 w-full" data-classes="py-2 px-4 text-sm border secondary-bg hover:bg-gray-200 hover:text-black text-white text-left cursor-pointer">
                                        <ul class="search-results-container w-full"></ul>
                                    </div>
                                </div>
                                <input id="search_location" name="location" class="autocomplete-location border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full" placeholder="{{ trans('placeholder.search_location') }}" autocomplete="off" data-search-results-container="search-results-container" value="{{ $location }}">
                                <input type="hidden" name="location_url" value="{{ $locationUrl }}">
                            </div>
                        @endif

                        {{-- Sale --}}
                        <div class="sale_price_filter @if(!$saleSelected) hidden @endif">
                            @include(themeViewPath('frontend.components.selects.price-select'), [
                                'mode' => 'sale',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'sale_min_price',
                                'name' => 'sale_min_price',
                                'prices' => $searchPriceRanges['sale'],
                                'initialOption' => trans('label.min_price'),
                                'currentValue' => $searchRequest->request->get('min_price'),
                            ])
                        </div>
                        <div class="sale_price_filter @if(!$saleSelected) hidden @endif">
                            @include(themeViewPath('frontend.components.selects.price-select'), [
                                'mode' => 'sale',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'sale_max_price',
                                'name' => 'sale_max_price',
                                'prices' => $searchPriceRanges['sale'],
                                'initialOption' => trans('label.max_price'),
                                'currentValue' => $searchRequest->request->get('max_price'),
                            ])
                        </div>

                        {{-- Rental --}}
                        <div class="rental_price_filter @if(!$rentalSelected) hidden @endif">
                            @include(themeViewPath('frontend.components.selects.price-select'), [
                                'mode' => 'rental',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'rental_min_price',
                                'name' => 'rental_min_price',
                                'prices' => $searchPriceRanges['rental'],
                                'initialOption' => trans('label.min_price'),
                                'currentValue' => $searchRequest->request->get('min_price'),
                            ])
                        </div>

                        <div class="rental_price_filter @if(!$rentalSelected) hidden @endif">
                            @include(themeViewPath('frontend.components.selects.price-select'), [
                                'mode' => 'rental',
                                'defaultRentalFrequency' => $defaultRentalFrequency,
                                'defaultCurrency' => $defaultCurrency,
                                'id' => 'rental_max_price',
                                'name' => 'rental_max_price',
                                'prices' => $searchPriceRanges['rental'],
                                'initialOption' => trans('label.max_price'),
                                'currentValue' => $searchRequest->request->get('max_price'),
                            ])
                        </div>

                        @if($bedroomFilterMode !== null)
                            @if($bedroomFilterMode === 'single')
                                <div>
                                    <select name="bedrooms" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                        <option class="text-black" value="">{{ trans('label.bedrooms') }}</option>
                                        @foreach($bedroomsRange as $bedroomCount)
                                            @php $selected = $bedroomCount != 0 && $bedroomCount == $searchRequest->get('bedrooms') ? ' selected="selected"' : ''; @endphp
                                            <option class="text-black" value="{{ $bedroomCount }}"{{ $selected }}>{{ trans('label.min') }} {{ $bedroomCount }}+ {{ trans('label.bedrooms') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @elseif($bedroomFilterMode === 'multi')
                                @php
                                    $minBedsValue = $maxBedsValue = null;
                                    if ($searchRequest->request->has('bedrooms')) {
                                          if (strpos($searchRequest->get('bedrooms'), '-') !== false) {
                                              $parts = explode('-', $searchRequest->get('bedrooms'));
                                              $minBedsValue = isset($parts[0]) && !empty($parts[0]) ? (int) $parts[0] : null;
                                              $maxBedsValue = isset($parts[1]) && !empty($parts[1]) ? (int) $parts[1] : null;
                                          } else {
                                              $minBedsValue = $searchRequest->get('bedrooms');
                                              $maxBedsValue = $searchRequest->get('bedrooms');
                                          }
                                      }
                                @endphp
                                <div>
                                    <select name="min_bedrooms" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                        <option value="">{{ trans('label.min').' '.trans('placeholder.bedrooms') }}</option>
                                        @foreach($bedroomsRange as $bedroomCount)
                                            @php $selected = $minBedsValue != 0 && $bedroomCount == $minBedsValue ? ' selected="selected"' : '' @endphp
                                            <option class="text-black" value="{{ $bedroomCount }}"{{ $selected }}>{{ trans('label.min') }} {{ $bedroomCount }} {{ trans('label.bedrooms') }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div>
                                    <select name="max_bedrooms" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                        <option value="">{{ trans('label.max').' '.trans('placeholder.bedrooms') }}</option>
                                        @foreach($bedroomsRange as $bedroomCount)
                                            @php $selected = $bedroomCount != 0 && $bedroomCount == $maxBedsValue ? ' selected="selected"' : '' @endphp
                                            <option class="text-black" value="{{ $bedroomCount }}"{{ $selected }}>{{ trans('label.max') }} {{ $bedroomCount }} {{ trans('label.bedrooms') }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            @endif
                        @endif

                        @if(isset($searchControls->bathrooms) && $searchControls->bathrooms)
                            <div>
                                <select name="bathrooms" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                    <option value="">{{ trans('label.min') }} {{ trans('placeholder.bathrooms') }}</option>
                                    @foreach($bathroomsRange as $bathroomCount)
                                        @php $selected = $searchRequest->request->get('bathrooms') != 0 && $searchRequest->request->get('bathrooms') == $bathroomCount ? ' selected="selected"' : '' @endphp
                                        <option class="text-black" value="{{ $bathroomCount }}"{{ $selected }}>{{ trans('label.min') }} {{ $bathroomCount }}+ {{ trans('label.bathrooms') }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(isset($searchControls->status) && $searchControls->status)
                            <div>
                                <select id="status" name="status"
                                        class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full">
                                    <option value="">Status</option>
                                    @foreach(\App\Models\Property::searchableTenderStatusList() as $tenderStatus)
                                        @php $selected = $searchRequest->request->get('status') == $tenderStatus ? ' selected="selected"' : '' @endphp
                                        <option class="text-black" value="{{ $tenderStatus }}"{{ $selected }}>{{ trans('property_status.'.strtolower($tenderStatus)) }}</option>
                                    @endforeach
                                </select>
                            </div>
                        @endif

                        @if(isset($searchControls->name) && $searchControls->name)
                            <div>
                                <input name="name" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full"
                                       placeholder="{{ trans('label.property_name') }}" value="{{ $searchRequest->request->get('name') }}">
                            </div>
                        @endif

                        @if(isset($searchControls->reference) && $searchControls->reference)
                            <div>
                                <input name="reference" class="border-b bg-transparent border-grey2 focus:border-0 border-opacity-30 focus:outline-none text-white py-3 w-full"
                                       placeholder="{{ trans('label.reference') }}" value="{{ $searchRequest->request->get('reference') }}">
                            </div>
                        @endif

                    </div>
                </div>

                <div class="lg:col-span-2 col-span-3 pl-0 xl:pl-6">
                    <h3 class="text-2xl font-medium text-white pt-8 lg:pt-0">{{ trans('label.property_type') }}</h3>
                    <div class="mt-8 grid grid-cols-2">
                        <div class="flex items-center mb-4">
                            <input id="property_type_all" type="checkbox" name="property_type[]" class="hidden" value="" @if($searchRequest->request->get('property_type') === null) checked="checked" @endif>
                            <label for="property_type_all" class="flex items-center cursor-pointer tracking-tight text-white fake-check" data-target="property_type_all">
                                <span class="w-4 h-4 inline-block mr-2 rounded-sm border-white border-2 flex-no-shrink"></span> {{ trans('generic.all') }}
                            </label>
                        </div>

                        @if(hasFeature(\App\Models\TenantFeature::FEATURE_USE_HEIRARCHICAL_CATEGORIES))
                            @foreach($propertyTypes as $propertyType)
                                @php
                                    $isChecked = is_array($searchRequest->request->get('property_type')) && in_array($propertyType->url_key, $searchRequest->request->get('property_type'))
                                @endphp

                                <div class="flex items-center mb-4">
                                    <input id="property_type_{{ $propertyType->url_key }}" type="checkbox" name="property_type[]" class="hidden" value="{{ $propertyType->url_key }}" @if($isChecked) checked="checked" @endif>
                                    <label for="property_type_{{ $propertyType->url_key }}" class="flex items-center cursor-pointer tracking-tight text-white fake-check"  data-target="property_type_{{ $propertyType->url_key }}">
                                        <span class="w-4 h-4 inline-block mr-2 rounded-sm border-white border-2 flex-no-shrink"></span> {{ transPropertyType($propertyType->name) }}
                                    </label>
                                </div>
                            @endforeach
                        @else
                            @foreach($propertyTypeCategories as $category)
                                @php
                                    $isChecked = is_array($searchRequest->request->get('property_type')) && in_array($category['category_slug'], $searchRequest->request->get('property_type'))
                                @endphp
                                <div class="flex items-center mb-4">
                                    <input id="property_type_{{ $category['category_slug'] }}" type="checkbox" name="property_type[]" class="hidden" value="{{ $category['category_slug'] }}" @if($isChecked) checked="checked" @endif>
                                    <label for="property_type_{{ $category['category_slug'] }}" class="flex items-center cursor-pointer tracking-tight text-white fake-check"  data-target="property_type_{{ $category['category_slug'] }}">
                                        <span class="w-4 h-4 inline-block mr-2 rounded-sm border-white border-2 flex-no-shrink"></span> {{ transPropertyType($category['category']) }}
                                    </label>
                                </div>
                            @endforeach
                        @endif
                    </div>

                    @if($searchableFeatures->count() > 0)
                        <h3 class="text-2xl font-medium text-white py-4 pb-8">{{ trans('header.property_features') }}</h3>
                        @include(themeViewPath('frontend.components.searchable-features'), [
                            'labelClasses' => 'text-white text-base',
                            'gridClasses' => 'grid-col-1 md:grid-cols-2 gap-2',
                        ])
                    @endif
                </div>
            </div>

            <div class="flex gap-x-3 mt-8">
                <span class="reset-search-filter-button text-xs text-center tracking-wide rounded-sm max-w-xs block py-3 px-8 cursor-pointer text-white border border-white" data-container=".full-screen-filter">{{ trans('generic.reset') }}</span>
                <input type="submit" class="outline-none border-0 text-xs text-center tracking-wide nav-text rounded-sm primary-border max-w-xs block py-3 px-8 transition-all cta hover:bg-hover hover:text-white hover:border-hover duration-500 cursor-pointer" value="{{ trans('label.search') }}">
            </div>
        </div>

        <input type="hidden" name="locale" value="{{ app()->getLocale() }}">
        @csrf

    </form>
</div>
