@php
$bedrooms = $property->bedrooms;
$bathrooms = str_replace('.0', '', $property->bathrooms);

if (!isset($additionalClasses)) {
    $additionalClasses = '';
}
@endphp

<span class="text-sm uppercase tracking-wider {{ $additionalClasses }}">
    @if($property->bedrooms > 0)
        {{ $bedrooms }} {{ trans('generic.bed') }}
    @endif
    @if ($property->bathrooms != '0')
        @if($property->bedrooms > 0) | @endif {{ $bathrooms }} {{ trans('generic.bath') }}
    @endif
</span>
