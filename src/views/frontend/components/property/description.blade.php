@php
    $propertyDescription = $property->descriptions->where('locale', app()->getLocale())->first();
    if ($propertyDescription === null) {
        $propertyDescription = $property->descriptions->first();
    }
@endphp

@if($propertyDescription !== null)
    <h3 class="text-2xl header-text mb-9 mt-10">{{ trans('header.property_description') }}</h3>
    <p class="leading-normal tracking-tight font-light mb-9">
        {!! optional($propertyDescription)->long !!}
    </p>
@endif
