@php
    use App\Services\Accreditation\Contracts\AccreditationServiceInterface;$accreditations = app()->make(AccreditationServiceInterface::class)->all('created_at', 'DESC', false);
    $count = $accreditations->count()
@endphp
@if($count > 0)
    <div class="border-t border-b mb-16">
        <section id="accreditations" class="py-10 overflow-x-hidden">
            <div class="swiper-wrapper md:justify-center">
                @php $i = 1 @endphp

                @foreach ($accreditations as $accreditation)
                    {{-- @php
                        $classes = $i !== $count ? 'block pb-8 md:pb-0 md:inline-block' : 'block md:inline-block';
                    @endphp --}}

                    <div class="bg-transparent swiper-slide md:px-8">
                        @if($accreditation->url !== null)
                            <a href="{{ $accreditation->url  }}" target="_BLANK">
                                <img src="{{ assetPath($accreditation->filepath) }}" class="h-11 w-auto object-contain mx-auto text-center" alt="{{ $accreditation->name }}">
                            </a>
                        @else
                            <img src="{{ assetPath($accreditation->filepath) }}" class="h-11 w-auto object-contain mx-auto text-center" alt="{{ $accreditation->name }}">
                        @endif
                    </div>
                    @php $i++ @endphp
                @endforeach
            </div>
            <!-- Swiper -->
            {{-- <div class="container mx-auto text-center flex flex-col md:flex-row items-center justify-between">

                @php $i = 1 @endphp

                @foreach ($accreditations as $accreditation)
                    @php
                        $classes = $i !== $count ? 'block pb-8 md:pb-0 md:inline-block' : 'block md:inline-block';
                    @endphp

                    <div class="bg-transparent {{ $classes }}">
                        @if($accreditation->url !== null)
                            <a href="{{ $accreditation->url  }}" target="_BLANK">
                                <img src="{{ assetPath($accreditation->filepath) }}" class="h-11 mx-auto text-center" alt="{{ $accreditation->name }}">
                            </a>
                        @else
                            <img src="{{ assetPath($accreditation->filepath) }}" class="h-11 mx-auto text-center" alt="{{ $accreditation->name }}">
                        @endif
                    </div>
                    @php $i++ @endphp
                @endforeach

            </div> --}}
        </section>
    </div>
@endif
