@php
    $i = 0;
    $saleProperties = $properties->where('tender_type', 'sale');
    $rentalProperties = $properties->where('tender_type', 'rental');

    $isMultiMode = $saleProperties->count() > 0 && $rentalProperties->count() > 0;
@endphp

@if ($properties->count() > 0)
    @php
        $header = trans('header.similar_properties');
        if (isset($customHeader)) {
            $header = $customHeader;
        }

        if (!isset($bgColor)) {
            $bgColor = 'bg-white';
        }

        if (!isset($card)) {
            $card = 'default';
        }

        if (!isset($additionalClasses)) {
            $additionalClasses = '';
        }
    @endphp

    <!-- ===================== Featured Properties ============================== -->
    <section class="py-16 lg:py-12 md:py-16 md:pb-24 {{ $bgColor }}">
        <div class="container mx-auto relative px-8 xl:px-0">
            <h3 class="text-2xl text-center md:pb-8 pb-4">{{ $header }}</h3>
            <div class="mx-auto text-center md:mx-0 md:text-left pb-4 md:pb-0">
                <div class="swiper-button-prevBtn relative md:absolute md:right-9 md:top-2 cursor-pointer inline-block pr-2 md:pr-0">
                    <img src="{{ themeImage('property/sarrow-left.svg') }}" class="svg-inject primary-text" alt="arrow" loading="lazy">
                </div>
                <div class="swiper-button-nextBtn relative md:absolute md:top-2 md:right-2 cursor-pointer inline-block">
                    <img src="{{ themeImage('property/sarrow-right.svg') }}" class="svg-inject primary-text" alt="arrow" loading="lazy">
                </div>
            </div>

            <div class="s-slider swiper mySwiperProperties overflow-hidden md:p-0 z-1 list-none">
                <div class="swiper-wrapper flex z-1 relative w-full transition-transform box-content ">
                    @foreach($properties as $property)
                        @if ($card === 'default')
                            @include(themeViewPath('frontend.components.cards.featured-property'), ['additionalClasses' => $additionalClasses])
                        @else
                            @include(themeViewPath("frontend.components.cards.{$card}"))
                        @endif
                    @endforeach
                </div>
            </div>
        </div>
    </section>


@endif
