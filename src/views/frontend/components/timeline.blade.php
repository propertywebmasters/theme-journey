@foreach ($items as $i => $item)
    @php
        $even = $i % 2 === 0;
    @endphp

    <div class="grid lg:grid-cols-2">
        <div style="border-color: #bcbcbc;" class="col-span-1 lg:border-r-2 {{ !$even ? 'hidden lg:grid' : '' }} {{ $loop->first ? 'pt-0 lg:pt-40' : 'pt-4 lg:pt-0' }} {{ $loop->last ? 'pb-10 lg:pb-40' : '' }}">
            @if ($even)
                @include(themeViewPath('frontend.components.cards.timeline'), ['item' => $item, 'left' => true, 'i' => $i])
            @endif
        </div>
        <div style="border-color: #bcbcbc;" class="col-span-1 lg:border-l-2 {{ $even ? 'hidden lg:grid' : '' }} {{ $loop->first ? 'lg:pt-40' : 'pt-4 lg:pt-0' }} {{ $loop->last ? 'pb-10 lg:pb-40' : '' }}">
            @if (!$even)
                @include(themeViewPath('frontend.components.cards.timeline'), ['item' => $item, 'i' => $i])
            @endif
        </div>
    </div>
@endforeach