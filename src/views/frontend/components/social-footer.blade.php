@php
use App\Models\SiteSetting;
use App\Models\SocialAccount;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;use App\Services\SocialAccount\Contracts\SocialAccountServiceInterface;

$service = app()->make(SocialAccountServiceInterface::class);
$socialAccounts = $service->get();

$socialCount = 0;
if ($socialAccounts !== null) {
    $networks = collect(SocialAccount::SOCIAL_SYSTEMS)->pluck('name')->toArray();

    foreach ($networks as $network) {
        if ($socialAccounts->$network !== null) {
            $socialCount++;
        }
    }
}

$siteName = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_NAME['key'], config('platform-cache.site_settings_enabled'))
@endphp


<section id="social-footer" class="text-xs footer-section py-4">
    <div class="container mx-auto lg:flex justify-between items-center px-8 xl:px-0">
        <div class="mb-4 lg:mb-0 text-center lg:text-left">
            <span class="text-xs font-light">&copy; Copyright {{ $siteName }} {{ date('Y') }}, All Rights Reserved.</span>
        </div>

        @if ($socialCount > 0)
            <div class="grid grid-cols-{{$socialCount}} gap-2 mb-4 lg:mb-0 items-end">
                @foreach($networks as $network)
                    @if($socialAccounts->$network !== null)
                    @php
                    // $height = $network === 'youtube' ? 'h-12' : 'h-10';
                    $height = $network === 'pinterest' ? 'h-5' : '';
                    $width = $network === 'youtube' ? 'w-5' : 'w-5';
                    $marginTop = $network === 'youtube' ? '-mt-1' : ''
                    @endphp
                    <div>
                        <a class="{{ $network }}" href="{{ $socialAccounts->$network }}" target="_blank" title="{{ $network }}">
                            <img src="{{ themeImage('icons/social/'.$network.'.svg') }}" class="svg-inject {{ $height }} {{ $width }} {{ $marginTop }} fill-current stroke-current primary-text cursor-pointer mx-auto" alt="{{ $network }}" loading="lazy">
                        </a>
                    </div>
                    @endif
                @endforeach
            </div>
        @endif

        @if(hasFeature(\App\Models\TenantFeature::FEATURE_SHOW_FOOTER_PW_LINK))
            <div class="mb-4 lg:mb-0 text-center lg:text-left">
                <span class="text-xs pw-copyright font-light">
                    <a class="font-bold hover:underline text-xs" href="{{ config('footer.pw_website_link') }}" target="_blank" title="Property Webmasters" rel="noopener">
                        {{ config('footer.pw_website_link_text') }}
                    </a> by Property Webmasters
                </span>
            </div>
        @endif
    </div>
</section>
