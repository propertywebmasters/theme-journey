@php
    $dataList = app()->make(App\Services\Search\Contracts\SearchServiceInterface::class)->dataListLocations();
    $locationValue = $searchRequest->request->get('location') !== null ? locationUrlStringToText($searchRequest->request->get('location')) : '';
@endphp

<style>
    datalist { display: block; }
</style>

<div class="location-datalist-container relative" style="">
    {{-- <input class="autocomplete-location w-full block focus:outline-none -ml-1 secondary-text {{ $border }} lg:pl-2 border-l-0 pl-0 text-sm location-datalist-input"  autocomplete="off" role="combobox" list="" name="location" placeholder="{{ trans('placeholder.search_location') }}"> --}}

    @include(themeViewPath('frontend.components.location-search-input'), [
        'datalist' => true, 
        'additionalClasses' => $border,
        'value' => $locationValue
    ])

    <datalist id="browsers" role="listbox" class="absolute w-full text-xs top-8 left-0" style="z-index: 999;">
        <div class="text-sm overflow-y-scroll overflow-x-hidden max-h-44 md:max-h-50 bg-white rounded-b-md">
            @php $i = 1 @endphp
            @foreach($dataList as $text => $value)
                <option class="text-xs bg-white text-black cursor-pointer hover:text-white hover-primary-bg py-2 px-4 @if($i === 1) pt-4 @endif" data-value="{{ $value }}">{{ $text }}</option>
                @php $i++ @endphp
            @endforeach
        </div>
    </datalist>

    <input type="hidden" name="location_url">
</div>
