<div class="relative z-10">
    <section id="home-hero" class="relative overflow-hidden" style="min-height: calc(100vh - 35px); max-height: calc(100vh - 35px);">
        @include(themeViewPath('frontend.components.hero.hero-slideshow'))

        <div id="hero-nav" class="absolute bottom-0 left-0 w-full">
            @if (hasFeature(\App\Models\TenantFeature::FEATURE_HERO_NAV) && $heroNavItems->count())
                <div class="container mx-auto">
                    <ul class="items-center justify-start text-white border-t hidden lg:flex">
                        @foreach ($heroNavItems as $i => $nav)
                            @php
                                $anchorLabel = getLocaleAnchorLabel($nav)
                            @endphp

                            <li class="py-5 relative {{ $nav->class }} {{ $i === 0 ? '' : 'pl-8' }}">
                                <a class="" href="{{ localeUrl($nav->url) }}" target="{{ $nav->target }}" title="Select {{ \Illuminate\Support\Str::slug($anchorLabel) }}">{{ $anchorLabel }}</a>
                            </li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @include(themeViewPath('frontend.components.banners.property-search-banner'), ['opacity' => '0.65'])
        </div>
    </section>
</div>
