@php
// if the slide is empty then we will inherit from theme default
if($slides->count() === 0) {
    $imagePath = \App\Support\ThemeManager::backgroundCSS('home.hero', tenantManager()->getTenant(), true);
    $slide = new \App\Models\Slide;
    $slide->image = $imagePath;
    $slide->title = optional(app()->make(\App\Services\Content\ContentBlockService::class)->getByIdentifier('home', 'hero-slogan', tenantManager()->getTenant(), app()->getLocale()))->content;
    $slides = collect()->push($slide);
}
@endphp

{{-- loop this --}}
<div id="home-slides" data-speed="12000">
    @php $i = 1; @endphp
    @foreach ($slides as $slide)
        @php
            $displayClass = $i === 1 ? '' : ' hidden';
            $active = $i === 1 ? 'active' : '';
        @endphp
        <div class="slide slide-{{$i}} {{ $active }} {{ $displayClass }}" data-slide-number="{{ $i }}">
            <div class="relative center-cover-bg min-h-screen bg-lazy-load flex items-center justify-center" data-style="background: linear-gradient(rgba(0, 0, 0, 0.3), rgba(0, 0, 0, 0.3)), url('{{ assetPath($slide->image) }}')">
                <div class="px-6 lg:px-0 pb-4 w-full container text-white">
                    <div class="lg:w-2/3">
                        @if (hasFeature(\App\Models\TenantFeature::FEATURE_SLIDE_SUBTITLES) && $slide->subtitle)
                            <p class="uppercase font-bold text-sm lg:text-base mb-2">{{ $slide->subtitle }}</p>
                        @endif
                        <h1 class="block text-white pb-6 text-5xl md:text-7xl text-left drop-shadow-lg">{!! $slide->title !!}</h1>
                        
                        @if (hasFeature(\App\Models\TenantFeature::FEATURE_SLIDE_GUIDE_PRICE) && $slide->guide_price)
                            <p class="mb-8">{{ trans('label.guide_price') }} {{ $slide->guide_price }}</p>
                        @endif

                        @if (hasFeature(\App\Models\TenantFeature::FEATURE_SLIDE_URL) && $slide->url)
                            <a href="{{ $slide->url }}" class="border rounded text-white px-4 py-2">
                                {{ trans('button.view_property') }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        @php $i++; @endphp
    @endforeach
</div>

