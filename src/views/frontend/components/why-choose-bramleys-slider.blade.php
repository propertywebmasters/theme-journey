<section class="bg-whiter">
    <div class="container mx-auto py-18 px-10 lg:p-32">
        <h1 class="text-6xl header-text text-center mb-16">Why choose Bramleys</h1>

        @php
            $data = [
                [
                    'title' => 'Accurate valuation advice',
                    'description' => 'With extensive knowledge in the property market to recommend a marketing strategy in line with market conditions',
                ],
                [
                    'title' => 'Qualified &amp; certified valuers',
                    'description' => 'All of our valuers are qualified by the NAEA or RICS',
                ],
                [
                    'title' => 'Open 7 days a week',
                    'description' => 'Being committed to selling your property means we need to be available when your potential buyers are.',
                ],
                [
                    'title' => 'Comprehensive marketing',
                    'description' => 'Giving you unrivalled exposure on the leading property portals, social media platforms and Bramleys website',
                ],
                [
                    'title' => 'Accurate valuation advice',
                    'description' => 'With extensive knowledge in the property market to recommend a marketing strategy in line with market conditions',
                ],
                [
                    'title' => 'Qualified &amp; certified valuers',
                    'description' => 'All of our valuers are qualified by the NAEA or RICS',
                ],
                [
                    'title' => 'Open 7 days a week',
                    'description' => 'Being committed to selling your property means we need to be available when your potential buyers are.',
                ],
                [
                    'title' => 'Comprehensive marketing',
                    'description' => 'Giving you unrivalled exposure on the leading property portals, social media platforms and Bramleys website',
                ],
            ]
        @endphp

        <div class="relative">
            <div class="mySwiperSellingWithBramleys overflow-hidden">
                <div class="swiper-wrapper w-full">
                    @foreach ($data as $datum)
                        <div class="swiper-slide text-center px-4">
                            <div class="mb-6">
                                <div class="bg-white rounded-full w-36 h-36 p-5 mx-auto">
                                    <img class="svg-inject h-auto w-full fill-current secondary-text mx-auto" src="{{ themeImage('icons/star-outline.svg') }}" alt="star-outline" loading="lazy">
                                </div>
                            </div>
                            <h5 class="text-xl mb-6">{!! $datum['title']  !!}</h5>
                            <p>{{ $datum['description'] }}</p>
                        </div>
                    @endforeach
                </div>
            </div>

            <div class="absolute inset-y-1/2 w-full -translate-y-1/2 flex justify-between lg:block">
                <div class="swiper-button-prevBtn relative md:absolute md:left-0 md:top-2 cursor-pointer inline-block pr-2 md:pr-0 -ml-8 lg:-ml-16">
                    <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject h-14 text-gray-400 fill-current stroke-current" alt="arrow" loading="lazy" style="transform: rotate(180deg);">
                </div>
                <div class="swiper-button-nextBtn relative md:absolute md:right-0 md:top-2 cursor-pointer inline-block -mr-8 lg:-mr-16">
                    <img src="{{ themeImage('icons/caret-right.svg') }}" class="svg-inject h-14 text-gray-400 fill-current stroke-current" alt="arrow" loading="lazy">
                </div>
            </div>
        </div>
    </div>
</section>
