@if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
<section class="bg-whiter py-7">
    <div class="container mx-auto px-4">
        <div class="text-center">
            <span class="inline-block text-primary lg:pr-5 mb-4 lg:mb-0">{!! dynamicContent($pageContents, 'listings-alert-divider-text') !!}</span>
            @if(user())
                <a href="{{ localeUrl('/account/alerts') }}" class="text-sm md:text-base text-center tracking-wide font-medium hover:text-white rounded max-w-xs px-7 py-3 inline-block hover:bg-hover transition-all cta duration-500">
                    {{ trans('search.create_property_alert') }}
                </a>
            @else
                <a href="javascript:" class="modal-button text-sm md:text-base text-center tracking-wide font-medium hover:text-white rounded max-w-xs px-7 py-3 inline-block hover:bg-hover transition-all cta duration-500" data-target="preauth-modal">
                    {{ trans('search.create_property_alert') }}
                </a>
            @endif
        </div>
    </div>
</section>
@endif
