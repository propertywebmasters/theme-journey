<section class="relative min-h-146">
    <div class="lg:absolute grid grid-cols-1 lg:grid-cols-2 w-full">
        <div class="center-cover-bg bg-lazy-load min-h-146 w-full" data-style="{{ backgroundCSSImage('home.about') }}"></div>

        <div class="tertiary-bg h-full w-full hidden lg:block">&nbsp;</div>
    </div>

    <div class="grid grid-cols-1 lg:grid-cols-2 container mx-auto lg:absolute left-1/2 top-1/2 transform lg:-translate-x-1/2 lg:-translate-y-1/2">
        <div class="hidden lg:block">&nbsp;</div>

        <div class="text-white tertiary-bg text-white py-18 p-8 lg:py-0 lg:pl-10 lg:pr-8 xl:pr-0">
            <h1 class="mb-6 lg:mb-10 header-text" style="font-size: 3.2rem; line-height: 1;">{!! translatableContent('home', 'about-title') !!}</h1>

            {!! translatableContent('home', 'about-text') !!}
        </div>
    </div>
</section>