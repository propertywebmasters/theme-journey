@php
    use App\Http\Controllers\Frontend\AboutController;
    use App\Http\Controllers\Frontend\ContactController;
    use App\Http\Controllers\Frontend\HomeController;
    use App\Http\Controllers\Frontend\ValuationController;
    use App\Models\SiteSetting;
    use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;

    // TODO: remove
    use App\Http\Controllers\Frontend\TmpController;

    $logoPath = assetPath(app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_LOGO['key'], config('platform-cache.site_settings_enabled')));
    $controller = get_class(request()->route()->getController());
    $method = request()->route()->getActionMethod();

    // only add transparency flag on homepage
    $headerClass = $controller === HomeController::class && $method === 'index'
        ? 'home'
        : 'navigation-bg';

    $headerClass = $controller === ValuationController::class && $method === 'index'
        ? 'valuation'
        : $headerClass;

    // $headerClass = $controller === ContactController::class && $method === 'index'
    //         ? 'contact'
    //         : $headerClass;

    $headerClass = $controller === AboutController::class && $method === 'index'
            ? 'about'
            : $headerClass;

    // TODO: remove
    $headerClass = $controller === TmpController::class && $method === 'selling'
            ? 'bg-transparent'
            : $headerClass;

    $headerClass = $controller === TmpController::class && $method === 'commercial'
            ? 'bg-transparent'
            : $headerClass;

    $currentCurrency = currentCurrency();

    $socialAccountService = app()->make(\App\Services\SocialAccount\Contracts\SocialAccountServiceInterface::class);
    $socialAccounts = $socialAccountService->get();

    $siteName = app()->make(\App\Services\SiteSetting\Contracts\SiteSettingServiceInterface::class)->retrieve(\App\Models\SiteSetting::SETTING_SITE_NAME['key'], config('platform-cache.site_settings_enabled'));

    $socialCount = 0;
    $networks = [];
    if ($socialAccounts !== null) {
        $networks = ['twitter', 'linked_in', 'facebook', 'instagram'];

        foreach ($networks as $network) {
            if ($socialAccounts->$network !== null) {
                $socialCount++;
            }
        }
    }

    $email = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);
@endphp

@if(env('CSS_BREAKPOINT_HELPER')) @include('frontend.components.helpers.breakpoint-helper') @endif

<div class="bg-white text-right">
    <div class="container mx-auto relative" style="max-width: 1280px !important;">
        <div class="absolute z-40 -top-4 left-4 xl:left-0">
            <a id="site-logo" class="cursor-pointer" title="Back to homepage" href="{{ localeUrl('/') }}">
                <img src="{{ $logoPath }}" class="@if (strtolower(getFileExtensionForFilename($logoPath)) === 'svg') svg-inject @endif @if (Request::is('/')) h-28 lg:h-34 @else h-28 @endif fill-current stroke-current primary-bg primary-text" alt="logo">
            </a>
        </div>

        <ul class="flex items-center justify-end py-2 px-4 lg:px-0">
            <li class="pr-5">
                <a class="text-white flex items-center text-sm" href="{{ localeUrl('/contact') }}">
                    <img class="svg-inject primary-text mr-2 h-4" src="{{ themeImage('contact/email2.svg') }}" alt="email">
                    <span class="text-sm text-black transition-all">{{ $email }}</span>
                </a>
            </li>
            <li class="pr-5">
                <a class="text-white flex items-center text-sm" href="tel:{{$siteTel}}">
                    <img class="svg-inject primary-text mr-2 h-4" src="{{ themeImage('contact/phone-alt.svg') }}" alt="phone">
                    <span class="text-sm text-black transition-all">{{ $siteTel }}</span>
                </a>
            </li>

            @if(hasFeature(\App\Models\TenantFeature::FEATURE_ACCOUNT_SYSTEM))
                @if (user())
                    <li class="inline-block">
                        <div class="flex items-center">
                            <div>
                                <img class="svg-inject h-4 w-auto fill-current primary-text mr-2" src="/img/icons/person.svg" alt="person" loading="lazy">
                                <div>
                                    <a class="relative py-3 text-sm cursor-pointer mr-4 inline mt-6 sm:mt-0 hover-lighten"
                                href="{{ localeUrl('/account') }}">{{ trans('auth.my_account') }}</a>
                                </div>
                            </div>
                        </div>
                    </li>
                @else
                    <li class="hidden lg:inline-block">
                        <div class="flex items-center">
                            <img class="svg-inject h-4 w-auto fill-current primary-text mr-2" src="/img/icons/person.svg" alt="person" loading="lazy">
                            <div>
                                <a class="modal-button relative py-3 text-sm cursor-pointer inline mt-6 sm:mt-0 hover-lighten" data-target="preauth-modal" href="javascript:">{{ trans('auth.login_register') }}</a>
                            </div>
                        </div>
                    </li>
                @endif
            @endif

            @foreach($networks as $network)
                @if($socialAccounts->$network === null)
                    @continue
                @endif

                <li class="inline-block mx-1">
                    <a href="{{ $socialAccounts->$network }}" target="_BLANK">
                        <img loading="lazy" src="{{ themeImage($network.'.svg') }}" class="svg-inject primary-text fill-current h-4" alt="{{ $network }}">
                    </a>
                </li>
            @endforeach
        </ul>
    </div>
</div>

<header id="navigation" class="absolute w-full flex flex-col z-30 mx-auto {{ $headerClass }}" style="left: 50%; transform: translateX(-50%);">
    <nav class="lg:pt-4 py-4 container mx-auto" style="max-width: 1280px !important;">
        <div class="flex lg:flex-row flex-col items-end lg:items-center lg:justify-end w-full">
            <div class="hidden lg:block">
                <a href="{{ localeUrl('/all-properties-for-sale') }}" class="block border text-white rounded py-2 px-4 mr-2 text-base">
                    Property search
                </a>
            </div>

            <div>
                <button class="text-white border px-4 py-2 text-sm rounded side-menu-button mr-2">
                    <div class="flex items-center">
                        <span class="mr-2 text-base">Menu</span>
                        <div class="tham tham-e-squeeze tham-w-6 bg-transparent focus:outline-none">
                            <div class="tham-box focus:outline-none">
                                <div class="tham-inner bg-white focus:outline-none"></div>
                            </div>
                        </div>
                    </div>
                </button>
            </div>

            <div class="hidden lg:block">
                <a id="get-a-valuation-button" class="capitalize-first-letter primary-bg text-white px-4 py-2 rounded block" href="{{ localeUrl('/valuation') }}">{{ trans('button.get_a_valuation') }}</a>
            </div>
        </div>
    </nav>
</header>

<div id="side-menu" class="fixed h-screen w-full lg:w-1/3 primary-bg-transparent z-40 top-0 text-white py-16" style="background-color: var(--ap-primary-transparent); backdrop-filter: blur(9px);">
    <div class="">
        <div class="text-right mb-8 pr-4 lg:pr-0 lg:w-3/4">
            <button class="rounded px-4 py-1 text-sm border close">
                <div class="flex items-center">
                    <span>Close</span>
                    {{-- <div>
                        <img class="block mx-auto w-8 h-8" src="{{ themeImage('icons/close.svg') }}">
                    </div> --}}
                </div>
            </button>
        </div>
        <ul>
            @foreach ($navigation as $nav)
                @php
                    $anchorLabel = getLocaleAnchorLabel($nav);
                    $activeClass = '';

                    if (Request::is(substr($nav->url, 1))) {
                        $activeClass = 'active';
                    }
                @endphp

                <li class="top-level mr-4 pl-8 py-2 block relative">
                    <div class="relative nav-text-container {{ $activeClass }}" style="padding-left: 2.875rem;">
                        <a class="nav-text {{ $nav->class }} {{ $activeClass }}" href="{{ $nav->children->count() ? 'javascript:;' : localeUrl($nav->url) }}" target="{{ $nav->target }}"
                            title="Select {{ \Illuminate\Support\Str::slug($anchorLabel) }}"
                            data-target="menu-{{ \Illuminate\Support\Str::slug($anchorLabel) }}">
                            {{ $anchorLabel }}
                        </a>
                    </div>
                    @if($nav->children->count() > 0)
                        <ul class="pl-10">
                            @foreach ($nav->children as $child)
                                @php
                                    $childAnchorLabel = getLocaleAnchorLabel($child)
                                @endphp
                                <li class="py-2">
                                    <div class="pl-6 relative child-nav-text-container ml-2">
                                        <a class="nav-text text-sm {{ $child->class }}" href="{{ localeUrl($child->url) }}"
                                            target="{{ $child->target }}">
                                            {{ $childAnchorLabel }}
                                        </a>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    @endif
                </li>
            @endforeach

            <li class="top-level mr-4 pl-8 py-2 block relative lg:hidden pb-2">
                <div class="flex items-center">
                    {{-- <img class="svg-inject h-4 w-auto fill-current primary-text mr-2" src="/img/icons/person.svg" alt="person" loading="lazy"> --}}
                    <div class="relative nav-text-container {{ $activeClass }}" style="padding-left: 2.875rem;">
                        <a class="relative py-3 cursor-pointer mr-4 inline mt-6 sm:mt-0 hover-lighten" href="{{ localeUrl('/valuation') }}">{{ trans('button.book_a_market_appraisal') }}</a>
                    </div>
                </div>
            </li>

            <li class="top-level mr-4 pl-8 py-2 block relative lg:hidden pb-2">
                <div class="flex items-center">
                    {{-- <img class="svg-inject h-4 w-auto fill-current primary-text mr-2" src="/img/icons/person.svg" alt="person" loading="lazy"> --}}
                    <div class="relative nav-text-container {{ $activeClass }}" style="padding-left: 2.875rem;">
                        <a class="modal-button relative py-3 cursor-pointer mr-4 inline mt-6 sm:mt-0 hover-lighten" data-target="preauth-modal" href="javascript:">{{ trans('auth.login_register') }}</a>
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
