@php
    $colourScheme = tenantManager()->getTenant()->colourScheme;
    $bgColor = '';

    if ($colourScheme) {
        list($r, $g, $b) = hexToRgb('#' . $colourScheme->primary);
        if (!isset($opacity)) {
            $opacity = 1;
        }
        $bgColor = "rgba({$r}, {$g}, {$b}, {$opacity})";
    }
@endphp

@php
    
@endphp

<div class="text-center primary-text" @if ($bgColor) style="background-color: {{ $bgColor }};" @endif>
    <a class="text-xl block py-8 text-white" href="{{ localeUrl('/all-properties-for-sale') }}" style="font-size: 1.3rem;">
        Begin your <span class="header-text text-white" style="font-weight: 700; font-size: 1.3rem;">Property Search</span> here
    </a>
</div>