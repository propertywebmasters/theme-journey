@if ($testimonials->count() > 0)
    <section id="testimonials" class="secondary-bg text-white">
        <div class="center-cover-bg bg-lazy-load w-full h-full py-16" data-style="{{ backgroundCSSImage('home.testimonials') }}">
            <div class="container mx-auto overflow-hidden px-8 xl:px-0">
                <div class="grid lg:grid-cols-{{ translatableContent('home', 'testimonials-accreditation-image') ? '3' : '2' }}">
                    @if (translatableContent('home', 'testimonials-accreditation-image'))
                        <div class="lg:col-span-1 h-full lg:border-r border-white items-center lg:pt-10 border-b mx-12 lg:mx-0 lg:border-b-0">
                            <div class="pb-10 lg:p-0">
                                <img src="{{ assetPath(translatableContent('home', 'testimonials-accreditation-image')) }}" alt="" class="w-60 h-auto object-contain">
                            </div>
                        </div>
                    @endif

                    <div class="lg:col-span-2 overflow-hidden p-10 px-0">
                        <div class="mySwiperTestimonial">
                            <!-- Additional required wrapper -->
                            <div class="swiper-wrapper">
                                <!-- Slides -->
                                @foreach ($testimonials as $testimonial)
                                    @include(themeViewPath('frontend.components.cards.testimonial'))
                                @endforeach
                            </div>
                            <!-- If we need pagination -->
                            {{-- <div class="swiper-pagination"></div> --}}

                            <div class="swiper-paginationT md:ml-24 mt-6"></div><!-- /.swiper-paginationT -->
            
                            <!-- If we need scrollbar -->
                            {{-- <div class="swiper-scrollbar"></div> --}}
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
@endif
