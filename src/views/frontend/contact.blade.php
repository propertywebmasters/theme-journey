@extends('layouts.app')

@section('content')

    @php
    use App\Models\SiteSetting;
    use App\Services\Branch\Contracts\BranchServiceInterface;use App\Services\SiteSetting\Contracts\SiteSettingServiceInterface;
    $siteEmail = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_EMAIL['key']);
    $siteTel = app()->make(SiteSettingServiceInterface::class)->retrieve(SiteSetting::SETTING_SITE_TEL['key']);
    $branches = app()->make(BranchServiceInterface::class)->all();
    $hasBranches = $branches->count() > 0
    @endphp

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    <section class="pt-44 py-22 center-cover-bg bg-lazy-load text-white" data-style="{{ backgroundCSSImage('contact.hero') }}">
        <div class="container mx-auto px-4 flex items-center flex-wrap">
            <div class="w-full lg:w-1/2 lg:pr-8">
                <h3 class="text-5xl leading-tight">{!! dynamicContent($pageContents, 'contact-title') !!}</h3>
                <div class="flex lg:mt-16 mt-6">
                    <div class="flex items-center justify-center sm:justify-start pr-4 md:pr-0">
                        <img class="svg-inject text-cta" src="{{ themeImage('contact/email2.svg') }}" alt="marker">
                        <a href="mailto:{{ $siteEmail }}" class="text-base text-right tracking-tight ml-2">{{ $siteEmail }}</a>
                    </div>
                    <div class="flex items-center px-0 md:px-10 py-3 md:py-0 justify-start sm:justify-center">
                        <img class="svg-inject text-cta" src="{{ themeImage('contact/phone-alt.svg') }}" alt="phone">
                        <a href="tel:{{ $siteTel }}" class="text-base text-right tracking-tight ml-2">{{ $siteTel }}</a>
                    </div>
                </div>
                @if($hasBranches)
                    <div class="lg:mt-12 mt-6">
                        <a class="text-sm text-center tracking-wide nav-text rounded max-w-xs inline-block transition-all hover:text-white duration-500 py-3 px-7 primary-bg" href="{{ localeUrl('/contact#contact-branches') }}">
                           {{ trans('contact.find_a_branch') }}</a>
                    </div>
                @endif
            </div>
            <div class="w-full lg:w-1/2 lg:mt-0 mt-11">
                <div class="shadow-md py-12 px-4 lg:px-12 bg-white">
                    @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6'])

                    <form action="{{ localeUrl('/contact') }}" method="post" class="recaptcha">
                        <input type="text" name="name" placeholder="{{ trans('contact.full_name') }}" class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
                        <input type="email" name="email" placeholder="{{ trans('contact.email_address') }}" class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
                        <input type="text" name="tel" placeholder="{{ trans('contact.telephone_number') }}" class="rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light">
                        <textarea name="message" id="" cols="30" rows="10" placeholder="{{ trans('contact.your_message') }}" class="rounded-sm bg-whiter h-32 w-full px-4 py-3 mb-3 font-light"></textarea>
                        @include(themeViewPath('frontend.forms.recaptcha.recaptcha-fallback-inputs'), ['inputClass' => 'rounded-sm bg-whiter h-14 w-full px-4 mb-3 font-light'])
                        <div class="text-center">
                            <button type="submit" class="capitalize-first-letter text-center tracking-wide text-white px-4 sm:px-9 rounded h-9 sm:h-12  right-1 top-1 hover:bg-hover transition-all duration-500 primary-bg">
                                {{ trans('contact.get_in_touch') }}
                            </button>
                        </div>
                        @csrf
                    </form>
                </div>
            </div>
        </div>
    </section>

    <!-- Branches -->
    @include(themeViewPath('frontend.components.contact-branches'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection


{{-- @extends('layouts.app')

@section('content')

    @include(themeViewPath('frontend.components.header'))

    <section id="contact" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('contact.hero') }}">
        <div class="pt-32 pb-8">

            <div class="container mx-auto transparent pt-7 px-8 lg:px-32 xl:px-64 pb-12 text-left">
                <div class="pt-7 pb-6 text-left">
                    <h1 class="text-white text-3xl md:text-5xl font-medium py-6 lg:w-4/5 lg:max-w-4/5 lg:pr-8 header-text">{!! dynamicContent($pageContents, 'contact-title') !!}</h1>
                </div>

                @include(themeViewPath('frontend.components.system-notifications'), ['customClass' => 'mb-6 -mt-12'])
                @include(themeViewPath('frontend.forms.contact-form'))
            </div>
        </div>



    </section>

    <div class="pt-12">
    @include(themeViewPath('frontend.components.contact-branches'))
    </div>

    @include(themeViewPath('frontend.components.footer'))

@endsection --}}
