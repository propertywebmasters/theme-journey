@php use App\Models\TenantFeature; @endphp
@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    @php $chunks = $properties->chunk(12) @endphp

    @include(themeViewPath('frontend.components.search-filter'))

    <section>
        <div class="container mx-auto px-4 xl:px-0">
            <div class="flex flex-col z-10 mb-5 pt-3">
                <div class="flex flex-col lg:flex-row w-full justify-between items-center py-4">
                    <div class="w-full lg:w-auto self-start sm:self-center flex md:flex-row flex-col md:flex-none  sm:flex-wrap justify-between items-center mb-4 lg:mb-0">
                        <div class="inline-block lg:w-auto md:w-full">
                            <span class="leading-loose tracking-tight text-lg">{!! $breadcrumbResultsString  !!} ({{ number_format($properties->total()) }})</span>
                        </div>
                    </div>

                    <div class="w-full lg:w-auto md:pt-4 lg:pt-0">
                        @include(themeViewPath('frontend.components.listings.view-modes'))
                        @include(themeViewPath('frontend.components.listings.search-options'))
                    </div>
                </div>
            </div>
        </div>
    </section>

    @php
        $cardViewFile = request()->get('list') == true ? 'property-list' : 'property';
        $gridCss = request()->get('list') == true ? 'grid grid-cols-1 gap-4' : 'grid md:grid-cols-2 lg:grid-cols-3 grid-cols-1 lg:gap-9 gap-4'
    @endphp

    @if($properties->count() > 0)
        <section class="mb-6">
            <div class="container mx-auto px-4 xl:px-0">
                <div class="{{ $gridCss }}">
                    @foreach($chunks[0] as $i => $property)
                        @include(themeViewPath('frontend.components.cards.'.$cardViewFile), ['property' => $property])

                        @if (!isset($chunks[1]) && hasFeature(TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === (array_key_last($chunks[0]->toArray()) - 1))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @else
        <div class="bg-gray-100 text-center p-12">
            <span class="text-2xl block font-medium pb-4 pb-4 mb-4">{{ trans('search.no_properties_found') }}</span>
            <a class="rounded primary-bg hover-lighten text-white py-4 md:mt-0 px-8 text-sm uppercase cursor-pointer block lg:inline-block capitalize-first-letter"
               href="{{ url()->current() }}#listings-search">{{ trans('search.no_results_button') }}</a>
        </div>
    @endif

    @if (isset($chunks[0], $chunks[1]) && !hasFeature(TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM))
        @include(themeViewPath('frontend.components.listings.listings-divider'))
    @else
        <!-- no properties here -->
    @endif

    @if(isset($chunks[1]))
        <section class="mb-6 mt-16">
            <div class="container mx-auto px-4 xl:px-0">
                <div class="{{ $gridCss }}">
                    @foreach($chunks[1] as $i => $property)
                        @include(themeViewPath('frontend.components.cards.'.$cardViewFile), ['property' => $property])

                        @if (hasFeature(TenantFeature::FEATURE_USE_CARD_LISTINGS_DIVIDER) && hasFeature(TenantFeature::FEATURE_ACCOUNT_SYSTEM) && $i === array_key_first($chunks[1]->toArray()))
                            @include(themeViewPath('frontend.components.cards.listings-divider'), ['list' => request()->get('list')])
                        @endif
                    @endforeach
                </div>
            </div>
        </section>
    @endif

    <section>
        <div class="container px-4 mx-auto mt-16 mb-16">
            <div class="flex justify-center items-center relative flex-col sm:flex-row ">
                <div>
                    @if($properties->previousPageUrl() !== null)
                        <a class="text-sm text-center tracking-wide rounded-full border border-activeCcolor max-w-xs block ml-4 py-3 px-16 transition-all hover:bg-activeCcolor hover:text-white text-active-color font-medium text-activeCcolor duration-500 inline-block"
                           href="{{ $properties->previousPageUrl() }}">{!! trans('pagination.previous')  !!}</a>
                    @endif
                    @if($properties->nextPageUrl() !== null)
                        <a class="text-sm text-center tracking-wide rounded-full border border-activeCcolor max-w-xs block ml-4 py-3 px-16 transition-all hover:bg-activeCcolor hover:text-white text-active-color font-medium text-activeCcolor duration-500 inline-block"
                           href="{{ $properties->nextPageUrl() }}">{!! trans('pagination.next')  !!}</a>
                    @endif
                </div>
            </div>
        </div>
    </section>

    @include(themeViewPath('frontend.components.search-page-content'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

    {{-- CTA Models --}}
    @include(themeViewPath('frontend.components.modals.create-alert'))
    @include(themeViewPath('frontend.components.modals.share-this-search'))

@endsection
