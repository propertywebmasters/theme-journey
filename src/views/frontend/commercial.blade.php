@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    <section id="commercial" class="center-cover-bg bg-lazy-load" data-style="{{ backgroundCSSImage('commercial.hero') }}">
        <div class="py-32 lg:py-48 relative">
            <div class="container mx-auto text-left px-8 lg:px-0">
                <div class="mb-8">
                    {{-- <h1 class="text-white text-5xl font-medium mx-auto py-6 header-text" style="font-size: 3.75rem;">{!! translatableContent('about', 'about-title') !!}</h1> --}}
                    <div class="">
                        <div class="mb-8">
                            <h1 class="text-white text-5xl font-medium mx-auto py-6">Commercial Properties</h1>
                            <p class="text-white">Providing expert advice for all of your commercial property needs</p>
                        </div>

                        @include(themeViewPath('frontend.components.home-search'), ['absolutelyPosition' => false])
                    </div>
                </div>
            </div>

            <div id="bottom-nav" class="container absolute bottom-0 left-1/2 w-full -translate-x-1/2 transform px-10 lg:px-0">
                <div class="border-t">
                    <div class="flex justify-between items-center text-white">
                        <div class="py-6 relative active">
                            <a class="text-sm" href="">Retail</a>
                        </div>
                        <div class="py-6 relative">
                            <a class="text-sm" href="">Industrial</a>
                        </div>
                        <div class="py-6 relative">
                            <a class="text-sm" href="">Offices</a>
                        </div>
                        <div class="py-6 relative">
                            <a class="text-sm" href="">Investment</a>
                        </div>
                        <div class="py-6 relative">
                            <a class="text-sm" href="">Redevelopment</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    {{-- <div class="primary-bg text-center text-white py-8">
        <a class="text-xl" href="{{ localeUrl('/all-properties-for-sale') }}">
            Begin your <span>Property Search</span> here
        </a>
    </div> --}}
    
    <section class="bg-whiter">
        <div class="container mx-auto p-8 py-20 lg:py-28 lg:px-0">
            <div class="grid lg:grid-cols-2 items-center gap-4 sm:gap-8 lg:gap-16">
                <div class="lg:col-span-1 lg:pb-16">
                    <h1 class="text-5xl header-text mb-8">Bramleys Commercial</h1>
                    <p>Bramleys industrial and commercial property team undertake agency and valuation work on properties across the Yorkshire Region, acting for clients including private individuals, investment companies, PLCs, housing associations and local authorities. The services offered by the commercial team, which includes qualified Chartered Surveyors include:</p>

                    {{-- <div class="mt-14">
                        <a class="rounded primary-bg text-white px-8 py-4" href="">
                            {{ trans('commercial.sell_with_us') }}
                        </a>
                    </div> --}}
                </div>

                <div class="lg:col-span-1 pl-16 center-cover-bg bg-lazy-load w-full h-104" data-style="{{ backgroundCSSImage('commercial.section-1') }}"></div>
            </div>

            <div class="grid lg:grid-cols-5 gap-4 mt-16">
                <div class="shadow rounded mb-8 lg:mb-0">
                    <div class="center-cover-bg bg-lazy-load w-full h-100 lg:h-52" data-style="{{ backgroundCSSImage('commercial.services-1') }}"></div>
                    <div class="text-center p-6">
                        <h5 class="text-xl">Sales and Lettings</h5>
                    </div>
                </div>

                <div class="shadow rounded mb-8 lg:mb-0">
                    <div class="center-cover-bg bg-lazy-load w-full h-100 lg:h-52" data-style="{{ backgroundCSSImage('commercial.services-2') }}"></div>
                    <div class="text-center p-6">
                        <h5 class="text-xl">Acquisitions</h5>
                    </div>
                </div>

                <div class="shadow rounded mb-8 lg:mb-0">
                    <div class="center-cover-bg bg-lazy-load w-full h-100 lg:h-52" data-style="{{ backgroundCSSImage('commercial.services-3') }}"></div>
                    <div class="text-center p-6">
                        <h5 class="text-xl">Property Management</h5>
                    </div>
                </div>

                <div class="shadow rounded mb-8 lg:mb-0">
                    <div class="center-cover-bg bg-lazy-load w-full h-100 lg:h-52" data-style="{{ backgroundCSSImage('commercial.services-4') }}"></div>
                    <div class="text-center p-6">
                        <h5 class="text-xl">Rent Review and Lease Renewal</h5>
                    </div>
                </div>

                <div class="shadow rounded mb-8 lg:mb-0">
                    <div class="center-cover-bg bg-lazy-load w-full h-100 lg:h-52" data-style="{{ backgroundCSSImage('commercial.services-5') }}"></div>
                    <div class="text-center p-6">
                        <h5 class="text-xl">Valuations</h5>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container mx-auto">
            @include(themeViewPath('frontend.components.featured-commercial-properties'), ['customHeader' => trans('header.featured_commercial_properties'), 'properties' => $featuredCommercialProperties])
        </div>
    </section>

    <section class="bg-whiter py-20 lg:py-28 px-10 lg:px-0">
        <div class="container mx-auto">
            <div class="pb-6 mb-12 lg:pb-4 lg:mb-8 border-b">
                <h3 class="text-3xl text-left">Contact one of our team</h3>
            </div>

            <div class="grid sm:grid-cols-2 lg:grid-cols-4 sm:gap-4 lg:gap-4">
                @foreach ($teamMembers as $teamMember)
                    @include(themeViewPath('frontend.components.cards.team-member'))
                @endforeach
            </div>
        </div>
    </section>

    <section>
        @include(themeViewPath('frontend.components.testimonials'))
    </section>

    @include(themeViewPath('frontend.components.footer'))
@endsection