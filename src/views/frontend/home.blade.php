@extends('layouts.app')

@section('content')

    @push('open-graph-tags')
        @include(themeViewPath('frontend.components.home-open-graph'))
    @endpush

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))

    {{-- Primary hero element --}}
    @include(themeViewPath('frontend.components.hero.hero-media'))

    {{-- Featured properties band --}}
    <div class="bg-white">
        @include(themeViewPath('frontend.components.featured-properties'), ['customHeader' => trans('header.this_weeks_featured_properties'), 'properties' => $featuredProperties])
    </div>

    {{-- About us band --}}
    @include(themeViewPath('frontend.components.about'))

    @include(themeViewPath('frontend.components.value-propositions'))

    {{-- Popular searches band --}}
    @include(themeViewPath('frontend.components.popular-searches'))

    @include(themeViewPath('frontend.components.get-valuation'))

    {{-- Latest news band --}}
    @include(themeViewPath('frontend.components.latest-news'))

    {{-- Testimonials band --}}
    @include(themeViewPath('frontend.components.testimonials'))

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
