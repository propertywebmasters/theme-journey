@extends('layouts.app')

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'), ['transparentNavigation' => false,])

    <!-- About -->
    <section class="bg-white pt-12">
        <div class="container py-8 pb-2 mx-auto px-4 lg:px-0">
            <div>
                <h2 class="text-2xl md:text-4xl pb-2 font-medium py-6">{{ $header }}</h2>
            </div>
            <hr class="mb-4">
            @include(themeViewPath('frontend.components.page-breadcrumbs'), ['navigation' => [
                [trans('header.home') => localeUrl('/')],
                [trans('header.latest_news') => null],
            ]])
        </div>
    </section>
    <!-- Content -->

    <section class="pt-2 pb-8">
        <div class="container mx-auto px-4 lg:px-0">
            <div class="lg:grid lg:grid-cols-2 gap-8">
                @foreach ($articles as $article)
                    @include(themeViewPath('frontend.components.cards.article-alt'), ['article' => $article, 'full_width' => $loop->first])
                @endforeach
            <div>
        </div>
    </section>
    <section>
        <div class="container px-4 mx-auto -mt-8 mb-16">
            <div class="flex justify-center items-center relative flex-col sm:flex-row ">

                @if($articles->previousPageUrl() !== null)
                    <a class="text-sm text-center tracking-wide rounded-full border border-activeCcolor max-w-xs block ml-4 py-3 px-16 transition-all hover:bg-activeCcolor hover:text-white text-active-color font-medium text-activeCcolor duration-500 inline-block"
                       href="{{ $articles->previousPageUrl() }}">{!! trans('pagination.previous')  !!}</a>
                @endif
                @if($articles->nextPageUrl() !== null)
                    <a class="text-sm text-center tracking-wide rounded-full border border-activeCcolor max-w-xs block ml-4 py-3 px-16 transition-all hover:bg-activeCcolor hover:text-white text-active-color font-medium text-activeCcolor duration-500 inline-block"
                       href="{{ $articles->nextPageUrl() }}">{!! trans('pagination.next')  !!}</a>
                @endif

            </div>
        </div>
    </section>


    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
