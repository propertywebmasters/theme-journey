@extends('layouts.app')

@push('open-graph-tags')
    @include(themeViewPath('frontend.components.article.open-graph'))
@endpush

@section('content')

    {{-- site header component, includes navigation --}}
    @include(themeViewPath('frontend.components.header'))
    <div class="tertiary-bg pt-32 pb-60" style="padding-top: calc(8rem - 18px);">

        <div class="container mx-auto transparent pt-7 px-8 lg:px-32 xl:px-64 pb-12 text-center">
            <h1 class="mx-auto py-6 text-white" style="font-size: 2.5rem;">{{ $article->title }}</h1>
            <span class="primary-text text-base pb-6 text-gray-400">{{ $article->created_at->format('jS F Y') }}</span>
        </div>
    </div>
    <div class="container mx-auto px-8 lg:px-0 -mt-60">
        <div class="bg-white relative">
            <img class="w-full h-150 shadow object-cover" src="{{ assetPath($article->image) }}" loading="lazy">
            <div class="text-sm py-4">
                <a href="{{ localeUrl('/news') }}" class="font-light text-sm"><img class="svg-inject h-4 primary-text inline-block pr-1" src="{{ themeImage('news/back-arrow.svg') }}" loading="lazy"> {{ trans('generic.back_to_news') }}</a>
            </div>

            <div class="relative">
                <div class="vertical-social-share hidden md:block absolute top-0">
                    @include(themeViewPath('frontend.components.social.vertical-share'), ['url' => route('news.show', [$article->url_key])])
                </div>
                @if ($article->subtitle !== null)
                    <h2 class="text-2xl font-bold mx-auto pt-4 ">{{ $article->subtitle }}</h2>
                @endif
                <p class="pt-4">{!! $article->content !!}</p>
                <hr class="my-6">
            </div>

            <div class="hidden md:hidden mx-auto text-center pb-2">
                <span class="header-text text-xl block">{{ trans('generic.share_this_article') }}</span>
                <div class="py-4">
                    @include(themeViewPath('frontend.components.social.share'), ['url' => localeUrl('/news/'.$article->url_key)])
                </div>
            </div>

        </div>

    </div>

    @include(themeViewPath('frontend.components.latest-news'), ['customHeader' => trans('header.similar_articles')])

    {{-- site footer --}}
    @include(themeViewPath('frontend.components.footer'))

@endsection
