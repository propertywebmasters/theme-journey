document.addEventListener("DOMContentLoaded", function (event) {
    
    new Swiper('.mySwiperSellingWithBramleys', {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,
        allowTouchMove: false,
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
        navigation: {
            prevEl: ".swiper-button-prevBtn",
            nextEl: ".swiper-button-nextBtn",
        },
        breakpoints: {
            "640": {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            "992": {
                slidesPerView: 4,
                spaceBetween: 20,
            }
        },
    });

    new Swiper('.mySwiperAwards', {
        slidesPerView: 2,
        spaceBetween: 30,
        loop: false,
        allowTouchMove: false,
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
        navigation: {
            prevEl: ".swiper-button-prevBtn-awards",
            nextEl: ".swiper-button-nextBtn-awards",
        },
        breakpoints: {
            "640": {
                slidesPerView: 3,
                spaceBetween: 10,
            },
            "992": {
                slidesPerView: 4,
                spaceBetween: 0,
            }
        },
    });

    var mySwiperProperties = new Swiper(".mySwiperCommercialProperties", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,
        allowTouchMove: false,
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
        navigation: {
            prevEl: ".swiper-button-prevBtn",
            nextEl: ".swiper-button-nextBtn",
        },
        breakpoints: {
            "640": {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            // "768": {
            //     slidesPerView: 1,
            //     spaceBetween: 10,
            // },
            "992": {
                slidesPerView: 3,
                spaceBetween: 20,
            }
        },
    });


    var mySwiperProperties = new Swiper(".mySwiperProperties", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,
        allowTouchMove: false,
        autoplay: {
            delay: 2500,
            disableOnInteraction: true,
        },
        navigation: {
            prevEl: ".swiper-button-prevBtn",
            nextEl: ".swiper-button-nextBtn",
        },
        breakpoints: {
            "640": {
                slidesPerView: 2,
                spaceBetween: 10,
            },
            // "768": {
            //     slidesPerView: 1,
            //     spaceBetween: 10,
            // },
            "992": {
                slidesPerView: 3,
                spaceBetween: 20,
            },
            "1024": {
                slidesPerView: 2,
                spaceBetween: 20,
            }
        },
    });

    var mySwiperPropertiesInner = new Swiper(".mySwiperPropertiesInner", {
        slidesPerView: 1,
        spaceBetween: 30,
        allowTouchMove: true,
        loop: false,
        // navigation: {
        //     prevEl: ".swiper-button-nextInner",
        //     nextEl: ".swiper-button-prevInner",
        // },

    });

    new Swiper('.mySwiperAbout', {
        slidesPerView: 1,
        spaceBetween: 30,
        allowTouchMove: true,
        loop: false,
        pagination: {
            el: ".swiper-paginationT",
            clickable: true,
        },
    });

    var mySwiperTestimonial = new Swiper(".mySwiperTestimonial", {
        slidesPerView: 1,
        spaceBetween: 30,
        loop: false,
        autoHeight: true,

        pagination: {
            el: ".swiper-paginationT",
            clickable: true,
        },
    });

    // property detail
    var swiperW = new Swiper(".mySwiperW", {
        slidesPerView: 1,
        loop: false,
        pagination: {
            el: ".swiper-pagination",
            type: "fraction",
        },
        navigation: {
            nextEl: ".swiper-arrow-right",
            prevEl: ".swiper-arrow-left",
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
            },
            "992": {
                slidesPerView: 2,
            },
            "1199": {
                slidesPerView: 2,
            }
        },
    });

    var swiperDoubleImage = new Swiper(".mySwiperDoubleImage", {
        slidesPerView: 2,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            type: "fraction",
        },
        navigation: {
            nextEl: ".swiper-arrow-right",
            prevEl: ".swiper-arrow-left",
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
            },
            "992": {
                slidesPerView: 2,
            },
            "1199": {
                slidesPerView: 2,
            }
        },
    });
    
    // property detail
    var swiperSingleImage = new Swiper(".mySwiperSingleImage", {
        slidesPerView: 1,
        loop: true,
        pagination: {
            el: ".swiper-pagination",
            type: "fraction",
        },
        navigation: {
            nextEl: ".swiper-arrow-right",
            prevEl: ".swiper-arrow-left",
        },
        breakpoints: {
            "768": {
                slidesPerView: 1,
            },
            "992": {
                slidesPerView: 1,
            },
            "1199": {
                slidesPerView: 1,
            }
        },
    });
    
    const sideMenu = document.getElementById('side-menu');
    if (sideMenu !== null) {
        const sideMenuButton = document.querySelector('.side-menu-button');
        const closeBtn = document.querySelector('#side-menu .close');

        if (sideMenuButton !== null) {
            sideMenuButton.addEventListener('click', function () {
                if (!sideMenu.classList.contains('open')) {
                    sideMenu.classList.add('open');
                }
            }, false);
        }

        if (closeBtn !== null) {
            closeBtn.addEventListener('click', function () {
                if (sideMenu.classList.contains('open')) {
                    sideMenu.classList.remove('open');
                }
            }, false);
        }
    }

    let smoothScroll = document.querySelectorAll('.smooth-scroll');

    if (smoothScroll.length) {
        smoothScroll[0].addEventListener('click', function () {
            const target = this.getAttribute('data-target');
            window.scrollTo({
                top: document.querySelector(target).offsetTop,
                behavior: 'smooth',
            });
        }, false);
    }

    const breakpoint = window.matchMedia('(min-width: 768px)');

    let accreditationsSwiper;
    
    const enableSwiper = function() {
        accreditationsSwiper = new Swiper('#accreditations', {
            slidesPerView: 3,
            spaceBetween: 30,
            loop: true,
            allowTouchMove: true,
            autoplay: {
                delay: 2500,
                disableOnInteraction: true,
            }
        });
     };

    const breakpointChecker = function() {
        if (breakpoint.matches === true) {
            if (accreditationsSwiper !== undefined) {
                accreditationsSwiper.destroy(true, true);
            }
            return;
        } else if (breakpoint.matches === false) {
           return enableSwiper();
        }
     };

     breakpoint.addEventListener('change', breakpointChecker);

     breakpointChecker();

    // smoothScroll = [...smoothScroll];
    
    // TODO: get this working
    // smoothScroll.forEach(function (element) {
    //     element.addEventListener('click', function () {
    //         const target = element.getAttribute('data-target');
    //         window.scrollTo(0, document.getElementById(target).offsetTop);
    //     }, false);
    // });
});